INSERT INTO Student(last_name, email, tel, first_name, date_birth, date_inscription)
VALUES
('Sponge','spongebob@mail.fr', '0102030405','bob', '2000-02-01', '2020-09-10'),
('Seize','seizelouis@mail.fr', '0901293857','louis', '2000-12-01', '2020-09-09'),
('Island','coneyisland@mail.fr', '0859383874','coney', '1999-02-01', '2020-09-11');

INSERT INTO Professor(first_name, last_name, email)
VALUES
('ProfMachin', 'Bidule', 'machinbidule@mail.fr'),
('ProfTruc', 'Pouit', 'trucpouit@mail.fr'),
('ProfProf', 'Prof', 'prof@mail.fr');

INSERT INTO Cycles(description, date_start, date_end)
VALUES 
('Cycle 1', '2020-10-01', '2020-03-31'),
('Cycle 2', '2020-03-31', '2020-09-01');

INSERT INTO Adress(city, country, street, zipcode, number)
VALUES 
('Lyon', 'France', 'rue machin', '69000', '2'),
('Lyon', 'France', 'rue truc', '69000', '10');

INSERT INTO CategorySubject(name, description)
VALUES
('Cat1', 'la catégorie 1'),
('Cat2', 'la catégorie 2'),
('Cat3', 'la catégorie 3')
;

INSERT INTO Room(name)
VALUES
('101'),
('201'),
('204'),
('505');

INSERT INTO AcademicLevel(level)
VALUES
('l1'),
('l2'),
('l3'),
('l1'),
('l2')
;

INSERT INTO Subject(max_students, description, nb_examens, nb_examens_continue, success_rate, Id_category)
VALUES
(30, 'blabla1', 3, 1, 12, 1),
(25, 'blabla2', 2, 2, 12, 2),
(30, 'blabla3', 4, 1, 10, 1)
;

INSERT INTO TeachingUnit(date_start, date_end, description, Id_Subject, Id_Cycles)
VALUES
('2020-10-01', '2020-12-15', 'TU blabla1', 1, 1),
('2020-12-16', '2021-02-15', 'TU blabla2', 1, 2),
('2021-02-16', '2020-05-31', 'TU blabla1', 1, 1);

INSERT INTO Course(period_start, period_end, Id_teachingUnit, Id_Professor)
VALUES
('2020-10-01', '2020-11-05', 1, 1),
('2020-10-01', '2020-12-01', 1, 1),
('2020-11-01', '2020-12-15', 2, 2);

INSERT INTO Examen(examen_date, type, Id_teachingUnit)
VALUES
('2020-12-16', 'oral', 1),
('2020-12-10', 'tp', 1),
('2021-02-20', 'group work', 2);

INSERT INTO Note(school_mark, score_max, is_valided, Id_Student, Id_examen)
VALUES
(14, 20, true, 1, 2),
(8, 20, false, 2, 2),
(17, 20, true, 2, 2);

INSERT INTO HalfDay(has_signed, date, Id_Room, Id_Student, Id_Course)
VALUES
(true, '2020-10-01', 2, 1, 1),
(true, '2020-12-01', 2, 2, 1),
(false, '2020-11-01', 2, 2, 3);

INSERT INTO habiter(Id_Student, Id_Adress)
VALUES
(1,2),
(2,2),
(3,1);

INSERT INTO Inscription(Id_Student, Id_teachingUnit)
VALUES
(1,1),
(1,2),
(2,3),
(2,1),
(3,2);

INSERT INTO detenir(Id_Student, Id_AcademicLevel, years_academic)
VALUES
(1,2,'2020'),
(2,1,'2020'),
(3,1,'2020');