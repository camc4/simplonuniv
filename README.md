# simplonUniv



## Description du projet

SimplonUniv est des leaders des universités en ligne avec le système Canadien cherche à avoir un outil de gestion de ses 2000 étudiants inscrits à jour. Il demande une application web accessible par ces gestionnaires d'étudiant pour faciliter le suivi et l'évolution des étudiants.

![badge](https://img.shields.io/badge/-langage-<YELLOW>)
![badge](https://img.shields.io/badge/-SQL-<ORANGE>)

## Workflow

Notre gitflow est organisé en deux branches principales : 
- developpement (branche de travail)
- production (branche de livraison)

Les caractéristiques de l'application seront ainsi proposées sur la branche développement, avec revue de code par les différents intervenants. 

![workflow](img/workflow.png)



## Normes de développement mises en place 

### Convention de nommage C#:

- class:   MaClass (PascalCasing)
- methode: CeciEstUneMethode (PascalCasing)
- variable: maVariableLocale (camelCasing)
- constante: MA_CONSTANTE (majuscules + underscores)


