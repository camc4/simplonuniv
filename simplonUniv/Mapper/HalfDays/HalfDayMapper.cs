using simplonUniv.Dtos.HalfDay;
using simplonUniv.Entities;

namespace Mapper.HalfDays;

public class HalfDayMapper
{
    public static DTOOnlyHalfDay TransformHalfDayToDto(simplonUniv.Entities.HalfDay halfDay)
    {
        DTOOnlyHalfDay halfDayRead = new DTOOnlyHalfDay()
        {
            id = halfDay.id
        };
        
        return halfDayRead;
    }
    
    public static simplonUniv.Entities.HalfDay TransformDtoToHalfDay(DTOOnlyHalfDay halfDay)
    {
        simplonUniv.Entities.HalfDay halfDayUpdate = new simplonUniv.Entities.HalfDay()
        {
            has_signed = halfDay.has_signed, 
            date = halfDay.date
        };
        
        return halfDayUpdate;
    }
}