using simplonUniv.Dtos.HalfDay;

namespace Mapper.HalfDay;

public static class MapperDTOOnlyHalfDay
{
    public static DTOOnlyHalfDay ToMapperDTOOnlyHalfDay(simplonUniv.Entities.HalfDay half)
    {
        return new DTOOnlyHalfDay()
        {
            id = half.id,
            has_signed = half.has_signed,
            date = half.date
        };
    }
    public static simplonUniv.Entities.HalfDay ToHalfDay(DTOOnlyHalfDay DTOhalf)
    {
        return new simplonUniv.Entities.HalfDay()
        {
            has_signed = DTOhalf.has_signed,
            date = DTOhalf.date
        };
    }
}