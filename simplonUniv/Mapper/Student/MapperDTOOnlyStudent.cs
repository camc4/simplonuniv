using simplonUniv.Dtos.Student;

namespace Mapper.Student;

public static class MapperDTOOnlyStudent
{
    public static DTOOnlyStudent ToMapperDTOOnlyStudent(simplonUniv.Entities.Student student)
    {
        return new DTOOnlyStudent()
        {
            id = student.id,
            first_name = student.first_name,
            last_name = student.last_name,
            email = student.email,
            tel = student.tel,
            date_birth = student.date_birth,
            date_inscription = student.date_inscription
        };
    }
    public static simplonUniv.Entities.Student ToStudent(DTOOnlyStudent DTOstudent)
    {
        return new simplonUniv.Entities.Student()
        {
            first_name = DTOstudent.first_name,
            last_name = DTOstudent.last_name,
            email = DTOstudent.email,
            tel = DTOstudent.tel,
            date_birth = DTOstudent.date_birth,
            date_inscription = DTOstudent.date_inscription
        };
    }
}