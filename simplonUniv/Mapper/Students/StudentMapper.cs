using simplonUniv.Dtos.Student;
using simplonUniv.Entities;

namespace Mapper.Students;

public class StudentMapper
{
    public static DTOOnlyStudent TransformStudentToDto(simplonUniv.Entities.Student student)
    {
        DTOOnlyStudent studentRead = new DTOOnlyStudent()
        {
            id = student.id
        };
        
        return studentRead;
    }
    
    public static simplonUniv.Entities.Student TransformDtoToStudent(DTOOnlyStudent student)
    {
        simplonUniv.Entities.Student studentUpdate = new simplonUniv.Entities.Student()
        {
            first_name = student.first_name,
            last_name = student.last_name,
            tel = student.tel,
            email = student.email,
            date_birth = student.date_birth,
            date_inscription = student.date_inscription
        };
        
        return studentUpdate;
    }
}