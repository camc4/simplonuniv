﻿using simplonUniv.Dtos.Course;
using simplonUniv.Entities;

namespace Mapper.Courses;

public static class CourseMapper
{
    public static DTOOnlyCourse TransformCourseToDto(Course course)
    {
        DTOOnlyCourse courseRead = new DTOOnlyCourse()
        {
            id = course.id
        };
        
        return courseRead;
    }
    
    public static Course TransformDtoToCourse(DTOOnlyCourse course)
    {
        Course courseUpdate = new Course()
        {
            period_start = course.period_start,
            period_end = course.period_end
        };
        
        return courseUpdate;
    }

}