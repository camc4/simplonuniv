using simplonUniv.Dtos.AcademicLevel;

namespace Mapper.AcademicLevel;

public static class MapperDTOOnlyAcademicLevel
{
    public static DTOOnlyAcademicLevel ToMapperDTOOnlyAcademicLevel(simplonUniv.Entities.AcademicLevel academic)
    {
        return new DTOOnlyAcademicLevel()
        {
            id = academic.id,
            level = academic.level
        };
    }
    public static simplonUniv.Entities.AcademicLevel ToAcademicLevel(DTOOnlyAcademicLevel DTOacademic)
    {
        return new simplonUniv.Entities.AcademicLevel()
        {
            level = DTOacademic.level
        };
    }
}