using simplonUniv.Dtos.Professor;

namespace Mapper.Professor;

public static class MapperDTOOnlyProfessor
{
    public static DTOOnlyProfessor ToMapperDTOOnlyProfessor(simplonUniv.Entities.Professor professor)
    {
        return new DTOOnlyProfessor()
        {
            id = professor.id,
            first_name = professor.first_name,
            last_name = professor.last_name,
            email = professor.email
        };
    }
    public static simplonUniv.Entities.Professor ToProfessor(DTOOnlyProfessor DTOprofessor)
    {
        return new simplonUniv.Entities.Professor()
        {
            first_name = DTOprofessor.first_name,
            last_name = DTOprofessor.last_name,
            email = DTOprofessor.email,
        };
    }
}