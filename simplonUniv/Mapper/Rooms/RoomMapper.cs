using simplonUniv.Dtos.Course;
using simplonUniv.Dtos.Room;
using simplonUniv.Entities;

namespace Mapper.Rooms;

public class RoomMapper
{
    public static DTOOnlyRoom TransformRoomToDto(Room room)
    {
        DTOOnlyRoom roomRead = new DTOOnlyRoom()
        {
            id = room.id
        };
        
        return roomRead;
    }
    
    public static Room TransformDtoToRoom(DTOOnlyRoom room)
    {
        Room roomUpdate = new Room()
        {
            name = room.name
        };
        
        return roomUpdate;
    }

}