using simplonUniv.Dtos.Adress;

namespace Mapper.Adress;

public static class MapperDTOOnlyAdress
{
    public static DTOOnlyAdress ToMapperDTOOnlyAdress(simplonUniv.Entities.Adress adress)
    {
        return new DTOOnlyAdress()
        {
            id = adress.id,
            city = adress.city,
            country = adress.country,
            zipcode = adress.zipcode,
            number = adress.number
        };
    }
    public static simplonUniv.Entities.Adress ToAdress(DTOOnlyAdress DTOadress)
    {
        return new simplonUniv.Entities.Adress()
        {
            city = DTOadress.city,
            country = DTOadress.country,
            zipcode = DTOadress.zipcode,
            number = DTOadress.number
        };
    }
}