using simplonUniv.Dtos.Room;

namespace Mapper.Exam;

public static class MapperDTOOnlyExam
{
    public static DTOOnlyExam ToMapperDTOOnlyExam(simplonUniv.Entities.Exam exam)
    {
        return new DTOOnlyExam()
        {
            id = exam.id,
            examen_date = exam.exam_date,
            type = exam.type
        };
    }
    public static simplonUniv.Entities.Exam ToExam(DTOOnlyExam DTOexam)
    {
        return new simplonUniv.Entities.Exam()
        {
            exam_date = DTOexam.examen_date,
            type = DTOexam.type
        };
    }
}