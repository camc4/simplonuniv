namespace simplonUniv.Dtos.Adress;

public class DTOOnlyAdress
{
    public int id { get; set; } 
    public string city { get; set; } = null!;
    public string country { get; set; } = null!;
    public string street { get; set; } = null!;
    public string? zipcode { get; set; }
    public string number { get; set; } = null!;
}