namespace simplonUniv.Dtos.Room;

public class DTOOnlyNote
{
    public int id { get; set; }
    public int school_mark { get; set; }
    public int score_max { get; set; }
    public bool? is_valided { get; set; }

}