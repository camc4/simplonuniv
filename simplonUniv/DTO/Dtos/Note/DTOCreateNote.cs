namespace simplonUniv.Dtos.Room;

public class DTOCreateNote
{
    public int IdStudent { get; set; }
    public int IdExam { get; set; }
    public int school_mark { get; set; }
    public int score_max { get; set; }
    public bool is_valided { get; set; }
}