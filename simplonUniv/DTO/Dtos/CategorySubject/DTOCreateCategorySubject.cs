namespace simplonUniv.Dtos.CategorySubject;

public class DTOCreateCategorySubject
{
    public string name { get; set; }
    
    public string description { get; set; }
}