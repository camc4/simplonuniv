namespace simplonUniv.Dtos.Subject;

public class DTOUpdateSubject
{
    public int id { get; set; }
    
    public int Id_category { get; set; }

    public int max_students { get; set; }

    public string? description { get; set; }
    
    public int nb_examens { get; set; }

    public int nb_examens_continue { get; set; }

    public decimal success_rate { get; set; }

}