namespace simplonUniv.Dtos.AcademicLevel;

public class DTOOnlyAcademicLevel
{
    public int id { get; set; }
    public string level { get; set; } = null!;

}