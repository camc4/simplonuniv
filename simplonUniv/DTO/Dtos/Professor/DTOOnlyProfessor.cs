namespace simplonUniv.Dtos.Professor;

public class DTOOnlyProfessor
{
    public int id { get; set; }
    public string first_name { get; set; } = null!;
    public string last_name { get; set; } = null!;
    public string? email { get; set; }

}