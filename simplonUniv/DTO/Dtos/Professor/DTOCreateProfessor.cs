namespace simplonUniv.Dtos.Professor;

public class DTOCreateProfessor
{
    public string first_name { get; set; } = null!;
    public string last_name { get; set; } = null!;
    public string? email { get; set; }
}