namespace simplonUniv.Dtos.Room;

public class DTOCreateExam
{
    public int Id_teachingUnit { get; set; }
    public DateOnly examen_date { get; set; }
    public string type { get; set; }
}