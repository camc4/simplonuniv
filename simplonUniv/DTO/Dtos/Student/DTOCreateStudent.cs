namespace simplonUniv.Dtos.Student;

public class DTOCreateStudent
{
    public string first_name { get; set; }
    public string last_name { get; set; }
    public string tel { get; set; }
    public string email { get; set; }
    public DateOnly? date_birth { get; set; }
}