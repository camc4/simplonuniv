namespace simplonUniv.Dtos.Student;

public class DTODisplayNameStudent
{
    public string first_name { get; set; }
    public string last_name { get; set; }
}