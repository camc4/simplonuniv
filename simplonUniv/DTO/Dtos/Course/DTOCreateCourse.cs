namespace simplonUniv.Dtos.Course;

public class DTOCreateCourse : DTOGenericCourse
{
    public int Id_teachingUnit { get; set; }

    public int Id_Professor { get; set; }
}