namespace simplonUniv.Dtos.Course;

public class DTOGenericCourse
{
    public DateOnly period_start { get; set; }

    public DateOnly period_end { get; set; }
}