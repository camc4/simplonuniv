using simplonUniv.Dtos.Student;

namespace simplonUniv.Dtos.TeachingUnit;

public class DTOTeachingUnitStudent
{
    public string? description { get; set; }

    public DateOnly date_start { get; set; }

    public DateOnly date_end { get; set; }
    
    public IEnumerable<DTODisplayNameStudent> students { get; set; }

}