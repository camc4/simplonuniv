namespace simplonUniv.Dtos.TeachingUnit;

public class DTOCreateTeachingUnit
{
    public int cycleId { get; set; }
    public int subjectId { get; set; }
    public DateOnly date_start { get; set; }

    public DateOnly date_end { get; set; }

    public string? description { get; set; }
}