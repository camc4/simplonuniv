namespace simplonUniv.Dtos.TeachingUnit;

public class DTOOnlyTeachingUnit
{
   
    public int id { get; set; }

    public DateOnly date_start { get; set; }

    public DateOnly date_end { get; set; }

    public string? description { get; set; }

}