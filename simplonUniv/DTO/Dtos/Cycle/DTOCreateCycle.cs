namespace simplonUniv.Dtos.Cycle;

public class DTOCreateCycle
{
    public DateOnly date_start { get; set; }

    public DateOnly date_end { get; set; }

    public string? description { get; set; }
}