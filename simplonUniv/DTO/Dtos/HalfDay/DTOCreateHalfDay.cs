namespace simplonUniv.Dtos.HalfDay;

public class DTOCreateHalfDay
{
    public bool has_signed { get; set; }
    public DateTime date { get; set; }
    public int Id_Room  { get; set; }
    public int Id_Student  { get; set; }
    public int Id_Course  { get; set; }
}