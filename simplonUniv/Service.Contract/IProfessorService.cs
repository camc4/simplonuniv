using System.Collections;
using simplonUniv.Dtos.Course;
using simplonUniv.Dtos.Professor;
using simplonUniv.Entities;

namespace Service.Contract;

public interface IProfessorService
{
    Task<IEnumerable> GetAllAsync();
    Task<object?> GetByKeyAsync(int id);
    Task<object> GetRelatedCourse(int id);
    Task<object> CreateElementAsync(DTOCreateProfessor newProfessor);
    Task<object> UpdateSudentt(DTOOnlyProfessor professor);
    Task<object> DeleteElement(Professor professor);
    Task<object> AssignToCourse(int idProfessor, int idCourse);
}