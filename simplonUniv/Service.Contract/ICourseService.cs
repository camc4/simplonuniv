using simplonUniv.Dtos.Course;
using simplonUniv.Entities;

namespace Service.Contract;

public interface ICourseService
{
    Task<List<DTOOnlyCourse>> GetAllAsync();
    
    Task<object?> GetByKeyAsync(int id);

    Task<object> CreateElementAsync(DTOCreateCourse newCourse);
    
    Task<object> UpdateElementAsync(DTOOnlyCourse course);
    
    Task<object> DeleteElementAsync(Course course);
}