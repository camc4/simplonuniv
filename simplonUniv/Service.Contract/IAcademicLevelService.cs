using System.Collections;
using simplonUniv.Dtos.AcademicLevel;
using simplonUniv.Dtos.Room;
using simplonUniv.Entities;

namespace Service.Contract;

public interface IAcademicLevelService
{
    Task<IEnumerable> GetAllAsync();
    Task<object> GetByKeyAsync(int id);
    Task<object> CreateElementAsync(DTOCreateAcademicLevel newAcademicLevel);
    Task<object> DeleteElement(AcademicLevel academicLevel);
    Task<object?> UpdateElement(DTOOnlyAcademicLevel academicLevel);
}