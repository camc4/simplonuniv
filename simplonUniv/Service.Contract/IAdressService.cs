using System.Collections;
using simplonUniv.Dtos.Adress;
using simplonUniv.Dtos.Course;
using simplonUniv.Dtos.Room;
using simplonUniv.Dtos.Student;
using simplonUniv.Entities;

namespace Service.Contract;

public interface IAdressService
{
    Task<IEnumerable> GetAllAsync();
    Task<object> GetByKeyAsync(int id);
    Task<object> CreateElementAsync(DTOCreateAdress newAdress);
    Task<object> DeleteElement(Adress adress);
    Task<object> UpdateElement(DTOOnlyAdress adress);
    Task<IEnumerable<DTOOnlyStudent>> GetRelatedStudent(int id);
    Task<IEnumerable<DTOOnlyStudent>> CreateAndAddToAdress(int id_adress, DTOCreateStudent student);
    Task<IEnumerable<DTOOnlyStudent>> DeleteStudentOfAdress(int id_adress, int idStudent);
}