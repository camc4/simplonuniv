using simplonUniv.Dtos.HalfDay;
using System.Collections;
using simplonUniv.Dtos.Room;
using simplonUniv.Entities;

namespace Service.Contract;

public interface IHalfDayService
{
    Task<IEnumerable> GetAllAsync();
    Task<object> GetByKeyAsync(int id);
    Task<object> CreateElementAsync(DTOCreateHalfDay newHalfDay);
    Task<object> DeleteElement(HalfDay exam);
    Task<object> UpdateElement(DTOOnlyHalfDay exam);
    Task<object> UpdateHalfdaySignedAsync(int id, int student_id);
}