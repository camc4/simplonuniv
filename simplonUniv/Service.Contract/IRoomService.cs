using System.Collections;
using simplonUniv.Dtos.Room;
using simplonUniv.Entities;

namespace Service.Contract;

public interface IRoomService
{
    Task<IEnumerable> GetAllAsync();
    Task<object?> GetByKeyAsync(int id);
    Task<object> CreateElementAsync(DTOCreateRoom room);
    Task<object> UpdateElementAsync(DTOOnlyRoom room);
    Task<object> DeleteElementAsync(Room room);
}