using System.Collections;
using simplonUniv.Dtos.Adress;
using simplonUniv.Dtos.Student;
using simplonUniv.Dtos.TeachingUnit;
using simplonUniv.Entities;

namespace Service.Contract;

public interface IStudentService
{
    Task<object> RegisterStudentToTeachingUnit(int idStudent, int idCourse);

    Task<IEnumerable<DTOOnlyStudent>> GetAllAsync();
    Task<object> GetByKeyAsync(int id);
    Task<IEnumerable<DTOOnlyAdress>> GetRelatedAdress(int id);
    Task<object> CreateElement(DTOCreateStudent student);
    Task<object> UpdateElement(DTOOnlyStudent modifiedsStudent);
    Task<object> DeleteElement(int id);
    Task<IEnumerable<DTOValidateTeachingUnit>> GetValidateTeachingUnit(int id);
}