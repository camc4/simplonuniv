using System.Collections;
using simplonUniv.Dtos.Course;
using simplonUniv.Dtos.Room;
using simplonUniv.Entities;

namespace Service.Contract;

public interface IExamService
{
    Task<object> GetAllAsync();
    Task<object> GetByKeyAsync(int id);
    Task<object> CreateElementAsync(DTOCreateExam newExam);
    Task<object> DeleteElement(Exam exam);
    Task<object?> UpdateElement(DTOOnlyExam exam);
}