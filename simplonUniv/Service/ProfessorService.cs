using System.Collections;
using Mapper.Professor;
using Repository.Contract;
using Service.Contract;
using simplonUniv.Dtos.Course;
using simplonUniv.Dtos.Professor;
using simplonUniv.Entities;

namespace Service;

public class ProfessorService : IProfessorService
{
    public readonly IRepoProfessor _repoProfessor;
    public readonly IRepoCourse _repoCourse;

    public ProfessorService(IRepoProfessor repoProfessor, IRepoCourse repoCourse)
    {
        _repoProfessor = repoProfessor;
        _repoCourse = repoCourse;
    }

    public async Task<IEnumerable> GetAllAsync()
    {
        var Professors = await _repoProfessor.GetAllAsync();

        List<DTOOnlyProfessor> result = new();
        foreach (var Professor in Professors)
        {
            result.Add(MapperDTOOnlyProfessor.ToMapperDTOOnlyProfessor(Professor));
        }

        return result;
    }

    public async Task<object?> GetByKeyAsync(int id)
    {
        var professor = await _repoProfessor.GetByKeyAsync(id);
        if (professor == null)
        {
            throw new Exception(message: "not found");
        }

        return MapperDTOOnlyProfessor.ToMapperDTOOnlyProfessor(professor);
    }

    public async Task<object> GetRelatedCourse(int id)
    {
        IEnumerable<Course> courses = (IEnumerable<Course>)await _repoProfessor.GetRelatedCourse(id);

        List<DTOOnlyCourse> result = new();
        foreach (var course in courses)
        {
            result.Add(new DTOOnlyCourse()
            {
                id = course.id,
                period_start = course.period_start,
                period_end = course.period_end

            });
        }

        return result;

    }

    public async Task<object> CreateElementAsync(DTOCreateProfessor professor)
    {
        if (professor == null)
        {
            throw new Exception("Professor is null");
        }

        Professor newProfessor = new Professor()
        {
            first_name = professor.first_name,
            last_name = professor.last_name,
            email = professor.email,
        };
        var createdProfessor = await _repoProfessor.CreateElementAsync(newProfessor);

        return MapperDTOOnlyProfessor.ToMapperDTOOnlyProfessor(createdProfessor);
    }

    public async Task<object> UpdateSudentt(DTOOnlyProfessor professor)
    {
        if (professor == null)
        {
            throw new Exception("Professor is null");
        }

        Professor newProfessor = MapperDTOOnlyProfessor.ToProfessor(professor);

        var modifiedsProfessor = await _repoProfessor.UpdateElementAsync(newProfessor);

        return MapperDTOOnlyProfessor.ToMapperDTOOnlyProfessor(modifiedsProfessor);
    }

    public async Task<object> DeleteElement(Professor professor)
    {
        if (professor == null)
        {
            throw new Exception("Professor is null");
        }

        var ret = await _repoProfessor.DeleteElementAsync(professor);

        return MapperDTOOnlyProfessor.ToMapperDTOOnlyProfessor(ret);

    }

    public async Task<object> AssignToCourse(int idProfessor, int idCourse)
    {
        var professor = await _repoProfessor.GetByKeyAsync(idProfessor);
        if (professor == null)
        {
            throw new Exception(message: "Professor not found");
        }

        var course = await _repoCourse.GetByKeyAsync(idCourse);
        if (course == null)
        {
            throw new Exception(message: "Course not found");
        }

        _repoProfessor.AssignToCourse(professor, course);

        return await GetRelatedCourse(professor.id);

    }
}