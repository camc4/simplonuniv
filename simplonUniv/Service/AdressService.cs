using System.Collections;
using Mapper.Adress;
using Mapper.Student;
using Repository.Contract;
using Service.Contract;
using simplonUniv.Dtos.Course;
using simplonUniv.Dtos.Adress;
using simplonUniv.Dtos.Room;
using simplonUniv.Dtos.Student;
using simplonUniv.Entities;

namespace Service;

public class AdressService : IAdressService
{
    public readonly IRepoAdress _repoAdress;
    public readonly IRepoStudent _repoStudent;

    public AdressService(IRepoAdress repoAdress, IRepoStudent repoStudent)
    {
        _repoAdress = repoAdress;
        _repoStudent = repoStudent;
    }
    /// <summary>
    /// Ressource pour récupérer une liste 
    /// </summary>
    /// <returns></returns>
    public async Task<IEnumerable> GetAllAsync()
    {
        var Adresss = await _repoAdress.GetAllAsync();

        List<DTOOnlyAdress> result = new();
        foreach (var Adress in Adresss)
        {
            result.Add(MapperDTOOnlyAdress.ToMapperDTOOnlyAdress(Adress));
        }

        return result;
    }
    /// <summary>
    /// Ressource pour récupérer une element par son id 
    /// </summary>
    /// <returns></returns>
    public async Task<object?> GetByKeyAsync(int id)
    {
        var adress = await _repoAdress.GetByKeyAsync(id);
        if (adress == null)
        {
            throw new Exception(message: "not found");
        }

        return MapperDTOOnlyAdress.ToMapperDTOOnlyAdress(adress);
    }
    
    /// <summary>
    /// Ressource pour cree un element 
    /// </summary>
    /// <returns></returns>
    public async Task<object> CreateElementAsync(DTOCreateAdress adress)
    {
        if (adress == null)
        {
            throw new Exception("Adress is null");
        }

        Adress newAdress = new Adress()
        {
            city = adress.city,
            number = adress.number,
            zipcode = adress.zipcode,
            country = adress.country
            
        };
        var createdAdress = await _repoAdress.CreateElementAsync(newAdress);

        return MapperDTOOnlyAdress.ToMapperDTOOnlyAdress(createdAdress);
    }
    /// <summary>
    /// Ressource pour modifier un element 
    /// </summary>
    /// <returns></returns>
    public async Task<object> UpdateElement(DTOOnlyAdress adress)
    {
        if (adress == null)
        {
            throw new Exception("Adress is null");
        }

        Adress newAdress = MapperDTOOnlyAdress.ToAdress(adress);

        var modifiedsAdress = await _repoAdress.UpdateElementAsync(newAdress);

        return MapperDTOOnlyAdress.ToMapperDTOOnlyAdress(modifiedsAdress);
    }

    public async Task<IEnumerable<DTOOnlyStudent>> GetRelatedStudent(int id)
    {
        Adress adress = (Adress)await _repoAdress.GetRelatedStudent(id);
        List<DTOOnlyStudent> listStudent = new();
       
        foreach (var student in adress.Id_Students)
        {
            listStudent.Add(MapperDTOOnlyStudent.ToMapperDTOOnlyStudent(student));
        }

        return listStudent;
    }

    public async Task<IEnumerable<DTOOnlyStudent>> CreateAndAddToAdress(int idAdress, DTOCreateStudent student)
    {
        Student newStudent = new Student()
        {
            first_name = student.first_name,
            last_name = student.last_name,
            email = student.email,
            tel = student.tel,
            date_birth = student.date_birth,
            date_inscription = DateOnly.FromDateTime(DateTime.Now)
        };
        var createdStudent = await _repoStudent.CreateElementAsync(newStudent);
        var gettedAdress = await _repoAdress.GetByKeyAsync(idAdress);

        await _repoAdress.AddToAdress(gettedAdress, createdStudent);

        return await GetRelatedStudent(idAdress);
    }
    
    public async Task<IEnumerable<DTOOnlyStudent>> DeleteStudentOfAdress(int idAdress, int idStudent)
    {
        _repoAdress.RemoveRelatedStudent(idAdress, idStudent);
        return await GetRelatedStudent(idAdress);
    }

    /// <summary>
    /// Ressource pour supprimer un element 
    /// </summary>
    /// <returns></returns>
    public async Task<object> DeleteElement(Adress adress)
    {
        if (adress == null)
        {
            throw new Exception("Adress is null");
        }

        var ret = await _repoAdress.DeleteElementAsync(adress);

        return MapperDTOOnlyAdress.ToMapperDTOOnlyAdress(ret);

    }
    
}