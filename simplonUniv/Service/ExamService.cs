

using Mapper.Exam;
using Repository.Contract;
using Service.Contract;
using simplonUniv.Dtos.Room;
using simplonUniv.Entities;

namespace Service;

public class ExamService : IExamService
{
    public readonly IRepoExam _repoExam;

    public ExamService(IRepoExam repoExam)
    {
        _repoExam = repoExam;
    }
    /// <summary>
    /// Ressource pour récupérer une liste 
    /// </summary>
    /// <returns></returns>
    public async Task<object> GetAllAsync()
    {
        var Exams = await _repoExam.GetAllAsync();

        List<DTOOnlyExam> result = new();
        foreach (var Exam in Exams)
        {
            result.Add(MapperDTOOnlyExam.ToMapperDTOOnlyExam(Exam));
        }

        return result;
    }
    /// <summary>
    /// Ressource pour récupérer une element par son id 
    /// </summary>
    /// <returns></returns>
    public async Task<object?> GetByKeyAsync(int id)
    {
        var exam = await _repoExam.GetByKeyAsync(id);
        if (exam == null)
        {
            throw new Exception(message: "not found");
        }

        return MapperDTOOnlyExam.ToMapperDTOOnlyExam(exam);
    }
    
    /// <summary>
    /// Ressource pour cree un element 
    /// </summary>
    /// <returns></returns>
    public async Task<object> CreateElementAsync(DTOCreateExam exam)
    {
        if (exam == null)
        {
            throw new Exception("Exam is null");
        }

        Exam newExam = new Exam()
        {
            exam_date = exam.examen_date,
            type = exam.type
        };
        var createdExam = await _repoExam.CreateElementAsync(newExam);

        return MapperDTOOnlyExam.ToMapperDTOOnlyExam(createdExam);
    }
    /// <summary>
    /// Ressource pour modifier un element 
    /// </summary>
    /// <returns></returns>
    public async Task<object> UpdateElement(DTOOnlyExam exam)
    {
        if (exam == null)
        {
            throw new Exception("Exam is null");
        }

        Exam newExam = MapperDTOOnlyExam.ToExam(exam);

        var modifiedsExam = await _repoExam.UpdateElementAsync(newExam);

        return MapperDTOOnlyExam.ToMapperDTOOnlyExam(modifiedsExam);
    }
    /// <summary>
    /// Ressource pour supprimer un element 
    /// </summary>
    /// <returns></returns>
    public async Task<object> DeleteElement(Exam exam)
    {
        if (exam == null)
        {
            throw new Exception("Exam is null");
        }

        var ret = await _repoExam.DeleteElementAsync(exam);

        return MapperDTOOnlyExam.ToMapperDTOOnlyExam(ret);

    }
    
}