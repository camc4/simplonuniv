using Mapper.Adress;
using Mapper.Student;
using Mapper.Students;
using Repository.Contract;
using Service.Contract;
using simplonUniv.Dtos.Adress;
using simplonUniv.Dtos.Student;
using simplonUniv.Dtos.TeachingUnit;
using simplonUniv.Entities;

namespace Service;

public class StudentService:IStudentService
{
    public readonly IRepoStudent _repoStudent;
    public readonly IRepoTeachingUnit _repoTeachingUnit;

    public StudentService(IRepoStudent repoStudent,IRepoTeachingUnit repoTeachingUnit)
    {
        _repoStudent = repoStudent;
        _repoTeachingUnit = repoTeachingUnit;
    }
    
    public async Task<object> RegisterStudentToTeachingUnit(int idStudent, int idTeachingUnit)
    {
        var student = await _repoStudent.GetByKeyAsync(idStudent);
        if (student == null)
        {
            throw new KeyNotFoundException("Student not found");
        }
        var teachingUnit = await _repoTeachingUnit.GetByKeyAsync(idTeachingUnit);
        if (teachingUnit == null)
        {
            throw new KeyNotFoundException("Teaching unit not found");
        }

        await _repoStudent.registerToTeachingUnit(teachingUnit,student);
        
        return teachingUnit;
    }
    public async Task<IEnumerable<DTOOnlyStudent>> GetAllAsync()
    {
        var students =  await _repoStudent.GetAllAsync();
        List<DTOOnlyStudent> result = new();
        foreach (var student in students)
        {
            result.Add(MapperDTOOnlyStudent.ToMapperDTOOnlyStudent(student));
        }

        return result;
    }

    public async Task<object> GetByKeyAsync(int id)
    {
        var student =  await _repoStudent.GetByKeyAsync(id);
        return MapperDTOOnlyStudent.ToMapperDTOOnlyStudent(student);
    }

    public async Task<IEnumerable<DTOOnlyAdress>> GetRelatedAdress(int id)
    {
        IEnumerable<Adress> adresses = await _repoStudent.GetRelatedAdress(id);
        List<DTOOnlyAdress> result = new();
        foreach (var adresse in adresses)
        {
            result.Add(MapperDTOOnlyAdress.ToMapperDTOOnlyAdress(adresse));
        }

        return result;
        
    }

    public async Task<object> CreateElement(DTOCreateStudent DTOStudent)
    {
        Student student = new Student()
        {
            first_name = DTOStudent.first_name,
            last_name = DTOStudent.last_name,
            tel = DTOStudent.tel,
            email = DTOStudent.email,
            date_birth = DTOStudent.date_birth,
            date_inscription = DateOnly.FromDateTime(DateTime.Now)
        };
        var newStudent = await _repoStudent.CreateElementAsync(student);

        return MapperDTOOnlyStudent.ToMapperDTOOnlyStudent(newStudent);
    }

    public async Task<object> UpdateElement(DTOOnlyStudent modifiedsStudent)
    {
        var updatedStudent = await _repoStudent.UpdateElementAsync(MapperDTOOnlyStudent.ToStudent(modifiedsStudent));
        return MapperDTOOnlyStudent.ToMapperDTOOnlyStudent(updatedStudent);
    }

    public async Task<object> DeleteElement(int id)
    {
        var student = await _repoStudent.DeleteElementAsync(await _repoStudent.GetByKeyAsync(id));
        return MapperDTOOnlyStudent.ToMapperDTOOnlyStudent(student);
    }

    public async Task<IEnumerable<DTOValidateTeachingUnit>> GetValidateTeachingUnit(int id)
    {
        var student = await _repoStudent.GetByKeyAsync(id);
        if (student == null)
        {
            throw new KeyNotFoundException("Student not found");
        }

        IEnumerable<TeachingUnit> teachingUnits = (IEnumerable<TeachingUnit>)await _repoStudent.GetRelatedTeachingUnit(id);
        List<DTOValidateTeachingUnit> result = new List<DTOValidateTeachingUnit>();
        foreach (var teachingUnit in teachingUnits)
        {
            int success = 0;
                
            foreach (var exam in teachingUnit.Exams)
            {
                if ((bool)exam.Notes.First().is_valided)
                {
                    success += 1;
                }
            }

            var avg = success / teachingUnit.Exams.Count;
            Subject subject = (Subject)_repoTeachingUnit.getRelatedSubject(teachingUnit);
            if (avg >= subject.success_rate)
            {
                result.Add(new DTOValidateTeachingUnit()
                {
                    date_start = teachingUnit.date_start,
                    date_end = teachingUnit.date_start,
                    description = teachingUnit.description,
                    id = teachingUnit.id,
                    is_validated = true
                });
            }
            
        }

        return result;

    }
    
}