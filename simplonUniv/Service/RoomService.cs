using System.Collections;
using Mapper.Courses;
using Mapper.Rooms;
using Repository.Contract;
using Service.Contract;
using simplonUniv.Dtos.Room;
using simplonUniv.Entities;

namespace Service;

public class RoomService : IRoomService
{

    public readonly IRepoRoom _repoRoom;

    public RoomService(IRepoRoom repoRoom)
    {
        _repoRoom = repoRoom;
    }
    public Task<IEnumerable> GetAllAsync()
    {
        throw new NotImplementedException();
    }

    public Task<object?> GetByKeyAsync(int id)
    {
        throw new NotImplementedException();
    }
    
    public async Task<object> CreateElementAsync(DTOCreateRoom room)
    {
        if (room == null)
        {
            throw new Exception("Room is null");
        }
        Room newRoom = new Room()
        {
            name = room.name
        };

        var createdRoom = await _repoRoom.CreateElementAsync(newRoom);
        return RoomMapper.TransformRoomToDto(createdRoom);
    }

    public async Task<object> UpdateElementAsync(DTOOnlyRoom room)
    {
        if (room == null)
        {
            throw new Exception("Room is null");
        }

        Room newRoom = RoomMapper.TransformDtoToRoom(room);

        var updatedRoom = await _repoRoom.UpdateElementAsync(newRoom);
        return RoomMapper.TransformRoomToDto(updatedRoom);
    }

    public async Task<object> DeleteElementAsync(Room room)
    {
        if (room == null)
        {
            throw new Exception("Room is null");
        }

        var ret = await _repoRoom.UpdateElementAsync(room);
        return RoomMapper.TransformRoomToDto(ret);
    }
}