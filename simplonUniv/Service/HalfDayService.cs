using System.Collections;
using Mapper.HalfDay;
using Mapper.HalfDays;
using Repository.Contract;
using Service.Contract;
using simplonUniv.Dtos.Course;
using simplonUniv.Dtos.HalfDay;
using simplonUniv.Dtos.Room;
using simplonUniv.Entities;

namespace Service;

public class HalfDayService : IHalfDayService
{
    
    public readonly IRepoHalfDay _repoHalfDay;

    public HalfDayService(IRepoHalfDay repoHalfDay)
    {
        _repoHalfDay = repoHalfDay;
    }
    /// <summary>
    /// Ressource pour récupérer une liste 
    /// </summary>
    /// <returns></returns>
    public async Task<IEnumerable> GetAllAsync()
    {
        var HalfDays = await _repoHalfDay.GetAllAsync();

        List<DTOOnlyHalfDay> result = new();
        foreach (var HalfDay in HalfDays)
        {
            result.Add(MapperDTOOnlyHalfDay.ToMapperDTOOnlyHalfDay(HalfDay));
        }

        return result;
    }
    /// <summary>
    /// Ressource pour récupérer une element par son id 
    /// </summary>
    /// <returns></returns>
    public async Task<object> GetByKeyAsync(int id)
    {
        var halfDay = await _repoHalfDay.GetByKeyAsync(id);
        if (halfDay == null)
        {
            throw new Exception(message: "not found");
        }

        return MapperDTOOnlyHalfDay.ToMapperDTOOnlyHalfDay(halfDay);
    }
    /// <summary>
    /// Ressource pour cree un element 
    /// </summary>
    /// <returns></returns>
    public async Task<object> CreateElementAsync(DTOCreateHalfDay halfDay)
    {
        if (halfDay == null)
        {
            throw new Exception("HalfDay is null");
        }

        HalfDay newHalfDay = new HalfDay()
        {
            has_signed = halfDay.has_signed,
            date = halfDay.date
        };
        var createdHalfDay = await _repoHalfDay.CreateElementAsync(newHalfDay);

        return MapperDTOOnlyHalfDay.ToMapperDTOOnlyHalfDay(createdHalfDay);
    }
    
    /// <summary>
    /// Ressource pour modifier un element 
    /// </summary>
    /// <returns></returns>
    public async Task<object> UpdateElement(DTOOnlyHalfDay halfDay)
    {
        if (halfDay == null)
        {
            throw new Exception("HalfDay is null");
        }

        HalfDay newHalfDay = MapperDTOOnlyHalfDay.ToHalfDay(halfDay);

        var modifiedsHalfDay = await _repoHalfDay.UpdateElementAsync(newHalfDay);

        return MapperDTOOnlyHalfDay.ToMapperDTOOnlyHalfDay(modifiedsHalfDay);
    }

    public async Task<object> DeleteElement(HalfDay halfDay)
    {
        if (halfDay == null)
        {
            throw new Exception("HalfDay is null");
        }

        var ret = await _repoHalfDay.DeleteElementAsync(halfDay);

        return MapperDTOOnlyHalfDay.ToMapperDTOOnlyHalfDay(ret);

    }
    
    public async Task<object> UpdateHalfdaySignedAsync(int id, int student_id)
    {
        var halfDay = await _repoHalfDay.GetByKeyAsync(id);
        if (halfDay == null)
        {
            throw new Exception("HalfDay is not found");
        }

        if (halfDay.Id_Student != student_id)
        {
            throw new Exception("Not the good student");
        }
        halfDay.has_signed = true; 
        halfDay.date = DateTime.Now;
        
        var updatedHalfDay = await _repoHalfDay.UpdateElementAsync(halfDay);

        return HalfDayMapper.TransformHalfDayToDto(updatedHalfDay);
    }
}