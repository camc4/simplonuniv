using Mapper.Courses;
using Repository.Contract;
using Service.Contract;
using simplonUniv.Dtos.Course;
using simplonUniv.Entities;

namespace Service;

public class CourseService : ICourseService
{
    public readonly IRepoCourse _repoCourse;
    
    public CourseService(IRepoCourse repositoryCourse)
    {
        _repoCourse = repositoryCourse;
    }

    
    public async Task<List<DTOOnlyCourse>> GetAllAsync()
    {
        var courses = await _repoCourse.GetAllAsync().ConfigureAwait(false);

        List<DTOOnlyCourse> courseDtos = new();

        foreach(var course in courses)
        {
            courseDtos.Add(CourseMapper.TransformCourseToDto(course));
        }

        return courseDtos;

    }

    public async Task<object?> GetByKeyAsync(int id)
    {
        var course = await _repoCourse.GetByKeyAsync(id).ConfigureAwait(false);
        if (course == null)
        {
            throw new Exception(message: "not found");
        }
        return CourseMapper.TransformCourseToDto(course);
    }

    public async Task<object> CreateElementAsync(DTOCreateCourse courseToAdd)
    {
        Course newCourse = new Course()
        {
            period_start = courseToAdd.period_start,
            period_end = courseToAdd.period_end,
        };

        var createdCourse = await _repoCourse.CreateElementAsync(newCourse).ConfigureAwait(false);

        return CourseMapper.TransformCourseToDto(createdCourse);

    }

    public async Task<object> UpdateElementAsync(DTOOnlyCourse course)
    {
        if (course == null)
        {
            throw new Exception("Course is null");
        }

        Course newCourse = CourseMapper.TransformDtoToCourse(course);

        var updatedCourse = await _repoCourse.UpdateElementAsync(newCourse);

        return CourseMapper.TransformCourseToDto(updatedCourse);

    }

    public async Task<object> DeleteElementAsync(Course course)
    {
        if (course == null)
        {
            throw new Exception("Course is null");
        }

        var ret = await _repoCourse.DeleteElementAsync(course);

        return CourseMapper.TransformCourseToDto(ret);

    }
}