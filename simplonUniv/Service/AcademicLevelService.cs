using System.Collections;
using Mapper.AcademicLevel;
using Repository.Contract;
using Service.Contract;
using simplonUniv.Dtos.Course;
using simplonUniv.Dtos.AcademicLevel;
using simplonUniv.Dtos.Room;
using simplonUniv.Entities;

namespace Service;

public class AcademicLevelService : IAcademicLevelService
{
    public readonly IRepoAcademicLevel _repoAcademicLevel;

    public AcademicLevelService(IRepoAcademicLevel repoAcademicLevel)
    {
        _repoAcademicLevel = repoAcademicLevel;
    }
    /// <summary>
    /// Ressource pour récupérer une liste 
    /// </summary>
    /// <returns></returns>
    public async Task<IEnumerable> GetAllAsync()
    {
        var AcademicLevels = await _repoAcademicLevel.GetAllAsync();

        List<DTOOnlyAcademicLevel> result = new();
        foreach (var AcademicLevel in AcademicLevels)
        {
            result.Add(MapperDTOOnlyAcademicLevel.ToMapperDTOOnlyAcademicLevel(AcademicLevel));
        }

        return result;
    }
    /// <summary>
    /// Ressource pour récupérer une element par son id 
    /// </summary>
    /// <returns></returns>
    public async Task<object?> GetByKeyAsync(int id)
    {
        var exam = await _repoAcademicLevel.GetByKeyAsync(id);
        if (exam == null)
        {
            throw new Exception(message: "not found");
        }

        return MapperDTOOnlyAcademicLevel.ToMapperDTOOnlyAcademicLevel(exam);
    }
    
    /// <summary>
    /// Ressource pour cree un element 
    /// </summary>
    /// <returns></returns>
    public async Task<object> CreateElementAsync(DTOCreateAcademicLevel exam)
    {
        if (exam == null)
        {
            throw new Exception("AcademicLevel is null");
        }

        AcademicLevel newAcademicLevel = new AcademicLevel()
        {
            level = exam.level
        };
        var createdAcademicLevel = await _repoAcademicLevel.CreateElementAsync(newAcademicLevel);

        return MapperDTOOnlyAcademicLevel.ToMapperDTOOnlyAcademicLevel(createdAcademicLevel);
    }
    /// <summary>
    /// Ressource pour modifier un element 
    /// </summary>
    /// <returns></returns>
    public async Task<object> UpdateElement(DTOOnlyAcademicLevel exam)
    {
        if (exam == null)
        {
            throw new Exception("AcademicLevel is null");
        }

        AcademicLevel newAcademicLevel = MapperDTOOnlyAcademicLevel.ToAcademicLevel(exam);

        var modifiedsAcademicLevel = await _repoAcademicLevel.UpdateElementAsync(newAcademicLevel);

        return MapperDTOOnlyAcademicLevel.ToMapperDTOOnlyAcademicLevel(modifiedsAcademicLevel);
    }
    /// <summary>
    /// Ressource pour supprimer un element 
    /// </summary>
    /// <returns></returns>
    public async Task<object> DeleteElement(AcademicLevel exam)
    {
        if (exam == null)
        {
            throw new Exception("AcademicLevel is null");
        }

        var ret = await _repoAcademicLevel.DeleteElementAsync(exam);

        return MapperDTOOnlyAcademicLevel.ToMapperDTOOnlyAcademicLevel(ret);

    }
    
}