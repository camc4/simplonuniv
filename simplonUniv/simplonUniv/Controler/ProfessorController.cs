using System.Collections;
using Microsoft.AspNetCore.Mvc;
using Repository.Contract;
using Service.Contract;
using simplonUniv.Dtos.Adress;
using simplonUniv.Dtos.Course;
using simplonUniv.Dtos.Professor;
using simplonUniv.Entities;


namespace simplonUniv.Controler
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProfessorController : ControllerBase
    {
        /// <summary>
        /// Le repository de gestion des Professor
        /// </summary>
        private readonly IProfessorService _serviceProfessor;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProfessorController"/> class.
        /// </summary>
        /// <param name="serviceProfessor">The repository Professor.</param>
        public ProfessorController(IProfessorService serviceProfessor)
        {
            _serviceProfessor = serviceProfessor;
        }

        // GET: api/Professor
        /// <summary>
        /// Ressource pour récupérer la liste des Professor
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        [ProducesResponseType(typeof(IEnumerable<DTOOnlyProfessor>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> ProfessorListAsync()
        {
            var Professors = await _serviceProfessor.GetAllAsync();
            return Ok(Professors);
        }
        // GET: api/Professor/{id}
        /// <summary>
        /// Ressource pour récupérer un Professor
        /// </summary>
        /// <returns></returns>
        [HttpGet(template: "{id}")]
        [ProducesResponseType(typeof(Professor), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetProfessorAsync(int id)
        {
            var Professors = await _serviceProfessor.GetByKeyAsync(id);
            return Ok(Professors);
        }
        // GET: api/Professor/{id}/Course
        /// <summary>
        /// Ressource pour récupérer les cours d'un Professor
        /// </summary>
        /// <returns></returns>
        [HttpGet(template: "{id}/Course")]
        [ProducesResponseType(typeof(IEnumerable<DTOOnlyCourse>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetProfessorCourseAsync(int id)
        {
            var courses = await _serviceProfessor.GetRelatedCourse(id);
            return Ok(courses);
        }
        
        // GET: api/Professor/{id}/Assign_to_a_course
        /// <summary>
        /// Action pour assigner un professor a un cours
        /// </summary>
        /// <returns></returns>
        [HttpPost(template: "{id}/Assign_to_a_course")]
        [ProducesResponseType(typeof(IEnumerable<DTOOnlyCourse>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> ProfessorAssignToCourse(int id,int idCourse)
        {
            var courses = await _serviceProfessor.AssignToCourse(id,idCourse);

            return Ok(courses);
        }
        [HttpPost()]
        [ProducesResponseType(typeof(DTOOnlyProfessor), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> ProfessorCreateAsync(DTOCreateProfessor Professor)
        {
            var createdProfessor = await _serviceProfessor.CreateElementAsync(Professor);
            return Ok(createdProfessor);
        }
        [HttpPut()]
        [ProducesResponseType(typeof(DTOOnlyProfessor), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> UpdateAsync(DTOOnlyProfessor Professor)
        {
            var modifiedsProfessor = await _serviceProfessor.UpdateSudentt(Professor);

            return Ok(modifiedsProfessor);
        }
        [HttpDelete()]
        [ProducesResponseType(typeof(Professor), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> DeleteProfessorAsync(Professor Professor)
        {
            if (Professor == null)
                return Problem("The id");

            var toReturn = await _serviceProfessor.DeleteElement(Professor);
            
            return Ok(toReturn);
        }

    }
}