using System.Collections;
using Microsoft.AspNetCore.Mvc;
using Repository.Contract;
using simplonUniv.Dtos.Adress;
using simplonUniv.Dtos.CategorySubject;
using simplonUniv.Dtos.Subject;
using simplonUniv.Entities;


namespace simplonUniv.Controler
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategorySubjectController : ControllerBase
    {
        /// <summary>
        /// Le repository de gestion des CategorySubject
        /// </summary>
        private readonly IRepoCategorySubject _repoCategorySubject;
        private readonly IRepoSubject _repoSubject;

        /// <summary>
        /// Initializes a new instance of the <see cref="CategorySubjectController"/> class.
        /// </summary>
        /// <param name="repoCategorySubject">The repository CategorySubject.</param>
        public CategorySubjectController(IRepoCategorySubject repoCategorySubject,IRepoSubject repoSubject)
        {
            _repoCategorySubject = repoCategorySubject;
            _repoSubject = repoSubject;
        }

        // GET: api/CategorySubject
        /// <summary>
        /// Ressource pour récupérer la liste des CategorySubject
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        [ProducesResponseType(typeof(IEnumerable<DTOOnlyCategorySubject>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> CategorySubjectListAsync()
        {
            var CategorySubjects = await _repoCategorySubject.GetAllAsync();
            List<DTOOnlyCategorySubject> result = new ();
            foreach(var CategorySubject in CategorySubjects)
            {
                result.Add(new DTOOnlyCategorySubject()
                {
                    id = CategorySubject.id,
                    name = CategorySubject.name,
                    description = CategorySubject.description
                });
            }
            
            return Ok(result);
        }
        [HttpGet(template: "{id}")]
        [ProducesResponseType(typeof(CategorySubject), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetCategorySubjectAsync(int id)
        {
            var CategorySubjects = await _repoCategorySubject.GetByKeyAsync(id);
            return Ok(CategorySubjects);
        }
        [HttpGet(template: "{id}/Subject")]
        [ProducesResponseType(typeof(IEnumerable<DTOOnlySubject>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetCategorySubjectSubjectAsync(int id)
        {
            IEnumerable<Subject> subjects = (IEnumerable<Subject>)await _repoCategorySubject.GetRelatedSubject(id);
            
            List<DTOOnlySubject> result = new ();
            foreach(var subject in subjects)
            {
                result.Add(new DTOOnlySubject()
                {
                    id = subject.id,
                    max_students = subject.max_students,
                    description = subject.description,
                    nb_examens = subject.nb_exams,
                    nb_examens_continue = subject.nb_exams_continue,
                    success_rate = subject.success_rate
                });
            }
            
            return Ok(result);
        }
        [HttpPost(template: "{id}/Subject")]
        [ProducesResponseType(typeof(IEnumerable<Adress>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> CreateCategorySubjectAdressAsync(int id,DTOSimpleSubject subject)
        {
            var CategorySubject = await _repoCategorySubject.GetByKeyAsync(id);
            if (CategorySubject == null)
            {
                return Problem("CategorySubject not found");
            }
        
            Subject newSubject = new Subject()
            {
                // BUG 
                Id_category = CategorySubject.id,
                max_students = subject.max_students,
                description = subject.description,
                nb_exams = subject.nb_examens,
                nb_exams_continue = subject.nb_examens_continue,
                success_rate = subject.success_rate
            };
            var createdSubject = await _repoSubject.CreateElementAsync(newSubject);
            
            return Ok(newSubject);
        }
        [HttpPost()]
        [ProducesResponseType(typeof(DTOOnlyCategorySubject), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> CategorySubjectCreateAsync(DTOCreateCategorySubject? CategorySubject)
        {
            if (CategorySubject == null)
            {
                return Problem("CategorySubject is null");
            }
            CategorySubject newCategorySubject = new CategorySubject()
            {
                name = CategorySubject.name,
                description = CategorySubject.description
            };

            var createdCategorySubject = await _repoCategorySubject.CreateElementAsync(newCategorySubject);
            return Ok(createdCategorySubject);
        }
        [HttpPut()]
        [ProducesResponseType(typeof(DTOOnlyCategorySubject), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> UpdateAsync(DTOOnlyCategorySubject CategorySubject)
        {
            if (CategorySubject == null)
            {
                return Problem("CategorySubject is null");
            }
            
            var modifiedsCategorySubject = await _repoCategorySubject.GetByKeyAsync(CategorySubject.id);
            
            if (modifiedsCategorySubject == null)
                return Problem("Does not exist");

            modifiedsCategorySubject.name = CategorySubject.name;
            modifiedsCategorySubject.description = CategorySubject.description;

            var toReturn = await _repoCategorySubject.UpdateElementAsync(modifiedsCategorySubject);
            
            return Ok(toReturn);
        }
        [HttpDelete()]
        [ProducesResponseType(typeof(CategorySubject), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> DeleteCategorySubjectAsync(CategorySubject CategorySubject)
        {
            if (CategorySubject == null)
                return Problem("The id");

            var toReturn = await _repoCategorySubject.DeleteElementAsync(CategorySubject);
            
            return Ok(toReturn);
        }

    }
}