using System.Collections;
using Microsoft.AspNetCore.Mvc;
using Repository.Contract;
using Service.Contract;
using simplonUniv.Dtos.Adress;
using simplonUniv.Dtos.Student;
using simplonUniv.Dtos.TeachingUnit;
using simplonUniv.Entities;


namespace simplonUniv.Controler
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        /// <summary>
        /// Le repository de gestion des student
        /// </summary>
        private readonly IStudentService _studentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentController"/> class.
        /// </summary>
        /// <param name="repoStudent">The repository student.</param>
        public StudentController(IStudentService studentService)
        {
            _studentService = studentService;
        }

        // GET: api/Student
        /// <summary>
        /// Ressource pour récupérer la liste des Student
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        [ProducesResponseType(typeof(IEnumerable<DTOOnlyStudent>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> StudentListAsync()
        {
            var students = await _studentService.GetAllAsync();
            return Ok(students);
        }
        [HttpGet(template: "{id}")]
        [ProducesResponseType(typeof(Student), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetStudentAsync(int id)
        {
            var student = await _studentService.GetByKeyAsync(id);
            return Ok(student);
        }
        [HttpGet(template: "{id}/Adress")]
        [ProducesResponseType(typeof(IEnumerable<DTOCreateAdress>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetStudentAdressAsync(int id)
        {
            IEnumerable<DTOOnlyAdress> adresses = await _studentService.GetRelatedAdress(id);
            
            return Ok(adresses);
        }
        [HttpGet(template: "{id}/Validate_teaching_unit")]
        [ProducesResponseType(typeof(IEnumerable<DTOValidateTeachingUnit>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetStudentValidateTeachingUnit(int id)
        {
            IEnumerable<DTOValidateTeachingUnit> teachingUnits = await _studentService.GetValidateTeachingUnit(id);
            
            return Ok(teachingUnits);
        }
        [HttpPost(template: "{id}/RegisterToTeachingUnit")]
        [ProducesResponseType(typeof(IEnumerable<Adress>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> RegisterStudentTeachingUnitAsync(int id,int idTeachingUnit)
        {
            try
            {
                var link = await _studentService.RegisterStudentToTeachingUnit(id, idTeachingUnit);
                return Ok(link);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return Problem(e.Message);
            }
        }
        [HttpPost()]
        [ProducesResponseType(typeof(DTOOnlyStudent), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> StudentCreateAsync(DTOCreateStudent student)
        {
            var createdStudent = await _studentService.CreateElement(student);
            return Ok(createdStudent);
        }
        [HttpPut()]
        [ProducesResponseType(typeof(DTOOnlyStudent), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> UpdateAsync(DTOOnlyStudent student)
        {
            var toReturn = await _studentService.UpdateElement(student);
            return Ok(toReturn);
        }
        [HttpDelete(template: "{id}")]
        [ProducesResponseType(typeof(Student), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> DeleteStudentAsync(int id)
        {
            var toReturn = await _studentService.DeleteElement(id);
            
            return Ok(toReturn);
        }

    }
}