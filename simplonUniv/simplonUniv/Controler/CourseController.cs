using Mapper.Courses;
using Microsoft.AspNetCore.Mvc;
using Repository.Contract;
using Service.Contract;
using simplonUniv.Dtos.Course;
using simplonUniv.Entities;


namespace simplonUniv.Controler
{
    [Route("api/[controller]")]
    [ApiController]
    public class CourseController : ControllerBase
    {
        /// <summary>
        /// Le repository de gestion des Course
        /// </summary>
        private readonly ICourseService _courseService;

        /// <summary>
        /// Initializes a new instance of the <see cref="CourseController"/> class.
        /// </summary>
        /// <param name="repoCourse">The repository Course.</param>
        public CourseController(ICourseService courseService)
        {
            _courseService = courseService;
        }

        // GET: api/Course
        /// <summary>
        /// Ressource pour récupérer la liste des Course
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        [ProducesResponseType(typeof(IEnumerable<DTOOnlyCourse>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> CourseListAsync()
        {
            var courses = await _courseService.GetAllAsync();
            return Ok(courses);
        }
        
        
        [HttpGet(template: "{id}")]
        [ProducesResponseType(typeof(Course), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetCourseAsync(int id)
        {
            var course = await _courseService.GetByKeyAsync(id);
            return Ok(course);
        }
        
        
        [HttpPost()]
        [ProducesResponseType(typeof(DTOOnlyCourse), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> CreateCourseAsync(DTOCreateCourse course)
        {
            var createdCourse = await _courseService.CreateElementAsync(course);
            return Ok(createdCourse);
        }
        [HttpPut()]
        [ProducesResponseType(typeof(DTOOnlyCourse), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> UpdateCourseAsync(DTOOnlyCourse course)
        {
            var updatedCourse = await _courseService.UpdateElementAsync(course);
            return Ok(updatedCourse);
        }
        [HttpDelete()]
        [ProducesResponseType(typeof(Course), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> DeleteCourseAsync(Course course)
        {
            var toReturn = await _courseService.DeleteElementAsync(course);
            return Ok(toReturn);
        }
    }
}