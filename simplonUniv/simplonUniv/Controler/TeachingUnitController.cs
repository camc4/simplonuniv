using System.Collections;
using Microsoft.AspNetCore.Mvc;
using Repository.Contract;
using simplonUniv.Dtos.Adress;
using simplonUniv.Dtos.Student;
using simplonUniv.Dtos.TeachingUnit;
using simplonUniv.Entities;


namespace simplonUniv.Controler
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeachingUnitController : ControllerBase
    {
        /// <summary>
        /// Le repository de gestion des TeachingUnit
        /// </summary>
        private readonly IRepoTeachingUnit _repoTeachingUnit;

        /// <summary>
        /// Initializes a new instance of the <see cref="TeachingUnitController"/> class.
        /// </summary>
        /// <param name="repoTeachingUnit">The repository TeachingUnit.</param>
        public TeachingUnitController(IRepoTeachingUnit repoTeachingUnit)
        {
            _repoTeachingUnit = repoTeachingUnit;
        }

        // GET: api/TeachingUnit
        /// <summary>
        /// Ressource pour récupérer la liste des TeachingUnit
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        [ProducesResponseType(typeof(IEnumerable<DTOOnlyTeachingUnit>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> TeachingUnitListAsync()
        {
            var TeachingUnits = await _repoTeachingUnit.GetAllAsync();
            List<DTOOnlyTeachingUnit> result = new ();
            foreach(var TeachingUnit in TeachingUnits)
            {
                result.Add(new DTOOnlyTeachingUnit()
                {
                    id = TeachingUnit.id,
                    description = TeachingUnit.description,
                    date_end = TeachingUnit.date_end,
                    date_start = TeachingUnit.date_start

                });
            }
            
            return Ok(result);
        }
        [HttpGet(template: "GetWithRelatedStudent")]
        [ProducesResponseType(typeof(IEnumerable<DTOOnlyTeachingUnit>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> AllTeachingUnitRelatedStudentListAsync()
        {
            IEnumerable<TeachingUnit>  TeachingUnits = (IEnumerable<TeachingUnit>)await _repoTeachingUnit.GetAllAndRelatedStudent();
            
            List<DTOTeachingUnitStudent> result_teaching = new ();
            foreach(var TeachingUnit in TeachingUnits)
            {
                List<DTODisplayNameStudent> result_student = new ();
                foreach (var student in TeachingUnit.Id_Students)
                {
                    result_student.Add(new DTODisplayNameStudent()
                    {
                        first_name = student.first_name,
                        last_name = student.last_name
                    });
                }
                result_teaching.Add(new DTOTeachingUnitStudent()
                {
                    description = TeachingUnit.description,
                    date_start = TeachingUnit.date_start,
                    date_end = TeachingUnit.date_end,
                    students = result_student
                });

            }
            
            return Ok(result_teaching);
        }
        [HttpGet(template: "{id}")]
        [ProducesResponseType(typeof(TeachingUnit), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetTeachingUnitAsync(int id)
        {
            var TeachingUnits = await _repoTeachingUnit.GetByKeyAsync(id);
            return Ok(TeachingUnits);
        }
        
        [HttpPost()]
        [ProducesResponseType(typeof(DTOOnlyTeachingUnit), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> TeachingUnitCreateAsync(DTOCreateTeachingUnit? TeachingUnit)
        {
            if (TeachingUnit == null)
            {
                return Problem("TeachingUnit not found");
            }
            TeachingUnit newTeachingUnit = new TeachingUnit()
            {
                Id_Cycle = TeachingUnit.cycleId,
                Id_Subject = TeachingUnit.subjectId,
                description = TeachingUnit.description,
                date_end = TeachingUnit.date_end,
                date_start = TeachingUnit.date_start
            };

            var createdTeachingUnit = await _repoTeachingUnit.CreateElementAsync(newTeachingUnit);
            return Ok("ok");
        }
        [HttpPut()]
        [ProducesResponseType(typeof(DTOOnlyTeachingUnit), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> UpdateAsync(DTOOnlyTeachingUnit TeachingUnit)
        {
            if (TeachingUnit == null)
            {
                return Problem("TeachingUnit is null");
            }
            
            var modifiedsTeachingUnit = await _repoTeachingUnit.GetByKeyAsync(TeachingUnit.id);
            
            if (modifiedsTeachingUnit == null)
                return Problem("Does not exist");

            modifiedsTeachingUnit.description = TeachingUnit.description;
            modifiedsTeachingUnit.date_end = TeachingUnit.date_end;
            modifiedsTeachingUnit.date_start = TeachingUnit.date_start;
            
            var toReturn = await _repoTeachingUnit.UpdateElementAsync(modifiedsTeachingUnit);
            
            return Ok(toReturn);
        }
        [HttpDelete()]
        [ProducesResponseType(typeof(TeachingUnit), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> DeleteTeachingUnitAsync(TeachingUnit TeachingUnit)
        {
            if (TeachingUnit == null)
                return Problem("The id");

            var toReturn = await _repoTeachingUnit.DeleteElementAsync(TeachingUnit);
            
            return Ok(toReturn);
        }

    }
}