using Microsoft.AspNetCore.Mvc;
using Repository.Contract;
using Service.Contract;
using simplonUniv.Dtos.Room;
using simplonUniv.Entities;


namespace simplonUniv.Controler
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoomController : ControllerBase
    {
        /// <summary>
        /// Le repository de gestion des Rooms
        /// </summary>
        private readonly IRoomService _roomService;

        /// <summary>
        /// Initializes a new instance of the <see cref="RoomController"/> class.
        /// </summary>
        /// <param name="repoRoom">The repository Room.</param>
        public RoomController(IRoomService roomService)
        {
            _roomService = roomService;
        }

        // GET: api/Rooms
        /// <summary>
        /// Ressource pour récupérer la liste des Rooms
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        [ProducesResponseType(typeof(IEnumerable<DTOOnlyRoom>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> RoomListAsync()
        {
            var rooms = await _roomService.GetAllAsync();
            return Ok(rooms);
        }
        [HttpGet(template: "{id}")]
        [ProducesResponseType(typeof(Room), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetRoomAsync(int id)
        {
            var room = await _roomService.GetByKeyAsync(id);
            return Ok(room);
        }
        
        
        [HttpPost()]
        [ProducesResponseType(typeof(DTOOnlyRoom), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> CreateRoomAsync(DTOCreateRoom room)
        {
            var createdCourse = await _roomService.CreateElementAsync(room);
            return Ok(createdCourse);
            
        }
        [HttpPut()]
        [ProducesResponseType(typeof(DTOOnlyRoom), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> UpdateRoomAsync(DTOOnlyRoom room)
        {
            var toReturn = await _roomService.UpdateElementAsync(room);
            
            return Ok(toReturn);
        }
        [HttpDelete()]
        [ProducesResponseType(typeof(Room), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> DeleteRoomAsync(Room room)
        {
            var toReturn = await _roomService.DeleteElementAsync(room);
            
            return Ok(toReturn);
        }
        

    }
}