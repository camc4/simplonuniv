using Microsoft.AspNetCore.Mvc;
using Service.Contract;
using simplonUniv.Dtos.HalfDay;
using simplonUniv.Entities;


namespace simplonUniv.Controler
{
    [Route("api/[controller]")]
    [ApiController]
    public class HalfDayController : ControllerBase
    {
        /// <summary>
        /// Le repository de gestion des HalfDay
        /// </summary>
        private readonly IHalfDayService _serviceHalfDay;

        /// <summary>
        /// Initializes a new instance of the <see cref="HalfDayController"/> class.
        /// </summary>
        /// <param name="serviceHalfDay">The repository HalfDay.</param>
        public HalfDayController(IHalfDayService serviceHalfDay)
        {
            _serviceHalfDay = serviceHalfDay;
        }

        // GET: api/HalfDay
        /// <summary>
        /// Ressource pour récupérer la liste des HalfDay
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        [ProducesResponseType(typeof(IEnumerable<DTOOnlyHalfDay>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> HalfDayListAsync()
        {
            var halfDays = await _serviceHalfDay.GetAllAsync();

            return Ok(halfDays);
        }
        [HttpGet(template: "{id}")]
        [ProducesResponseType(typeof(HalfDay), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetHalfDayAsync(int id)
        {
            var HalfDays = await _serviceHalfDay.GetByKeyAsync(id);
            return Ok(HalfDays);
        }
        [HttpPost()]
        [ProducesResponseType(typeof(DTOOnlyHalfDay), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> HalfDayCreateAsync(DTOCreateHalfDay halfDay)
        {
            
            var createdHalfDay = await _serviceHalfDay.CreateElementAsync(halfDay);
            return Ok(createdHalfDay);
        }
        [HttpPut()]
        [ProducesResponseType(typeof(DTOOnlyHalfDay), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> UpdateAsync(DTOOnlyHalfDay halfDay)
        {
            
            var toReturn = await _serviceHalfDay.UpdateElement(halfDay);
            return Ok(toReturn);
        }
        [HttpDelete()]
        [ProducesResponseType(typeof(HalfDay), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> DeleteHalfDayAsync(HalfDay halfDay)
        {
            var toReturn = await _serviceHalfDay.DeleteElement(halfDay);
            return Ok(toReturn);
        }
        
        [HttpPut(template: "{id}/Updated_after_signature_of_a_student/{student_id}")]
        [ProducesResponseType(typeof(DTOOnlyHalfDay), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> UpdateHalfDayAfterSignatureStudentAsync(int id, int student_id)
        {
            var updatedHalfDay = await _serviceHalfDay.UpdateHalfdaySignedAsync(id, student_id);
            
            return Ok(updatedHalfDay);
        }

    }
}