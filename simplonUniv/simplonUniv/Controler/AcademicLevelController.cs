using System.Collections;
using Microsoft.AspNetCore.Mvc;
using Repository.Contract;
using Service.Contract;
using simplonUniv.Dtos.Adress;
using simplonUniv.Dtos.Course;
using simplonUniv.Dtos.AcademicLevel;
using simplonUniv.Entities;


namespace simplonUniv.Controler
{
    [Route("api/[controller]")]
    [ApiController]
    public class AcademicLevelController : ControllerBase
    {
        /// <summary>
        /// Le repository de gestion des AcademicLevel
        /// </summary>
        private readonly IAcademicLevelService _academicLevelService;

        /// <summary>
        /// Initializes a new instance of the <see cref="AcademicLevelController"/> class.
        /// </summary>
        /// <param name="academicLevelService">The repository AcademicLevel.</param>
        public AcademicLevelController(IAcademicLevelService academicLevelService)
        {
            _academicLevelService = academicLevelService;
        }

        // GET: api/AcademicLevel
        /// <summary>
        /// Ressource pour récupérer la liste des AcademicLevel
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        [ProducesResponseType(typeof(IEnumerable<DTOOnlyAcademicLevel>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> AcademicLevelListAsync()
        {
            var academicLevels = await _academicLevelService.GetAllAsync();
            return Ok(academicLevels);
        }
        [HttpGet(template: "{id}")]
        [ProducesResponseType(typeof(AcademicLevel), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetAcademicLevelAsync(int id)
        {
            var academicLevels = await _academicLevelService.GetByKeyAsync(id);
            return Ok(academicLevels);
        }
        
        [HttpPost()]
        [ProducesResponseType(typeof(DTOOnlyAcademicLevel), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> AcademicLevelCreateAsync(DTOCreateAcademicLevel academicLevel)
        {
            var createdAcademicLevel = await _academicLevelService.CreateElementAsync(academicLevel);
            return Ok(createdAcademicLevel);
        }
        [HttpPut()]
        [ProducesResponseType(typeof(DTOOnlyAcademicLevel), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> UpdateAsync(DTOOnlyAcademicLevel academicLevel)
        {
            var toReturn = await _academicLevelService.UpdateElement(academicLevel);
            
            return Ok(toReturn);
        }
        [HttpDelete()]
        [ProducesResponseType(typeof(AcademicLevel), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> DeleteAcademicLevelAsync(AcademicLevel academicLevel)
        {
            var toReturn = await _academicLevelService.DeleteElement(academicLevel);
            
            return Ok(toReturn);
        }

    }
}