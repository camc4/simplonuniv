using System.Collections;
using Microsoft.AspNetCore.Mvc;
using Repository.Contract;
using simplonUniv.Dtos.Adress;
using simplonUniv.Dtos.Subject;
using simplonUniv.Dtos.TeachingUnit;
using simplonUniv.Entities;


namespace simplonUniv.Controler
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubjectController : ControllerBase
    {
        /// <summary>
        /// Le repository de gestion des Subject
        /// </summary>
        private readonly IRepoSubject _repoSubject;

        /// <summary>
        /// Initializes a new instance of the <see cref="SubjectController"/> class.
        /// </summary>
        /// <param name="repoSubject">The repository Subject.</param>
        public SubjectController(IRepoSubject repoSubject)
        {
            _repoSubject = repoSubject;
        }

        // GET: api/Subject
        /// <summary>
        /// Ressource pour récupérer la liste des Subject
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        [ProducesResponseType(typeof(IEnumerable<DTOOnlySubject>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> SubjectListAsync()
        {
            var Subjects = await _repoSubject.GetAllAsync();
            List<DTOOnlySubject> result = new ();
            foreach(var Subject in Subjects)
            {
                result.Add(new DTOOnlySubject()
                {
                    id = Subject.id,
                    max_students = Subject.max_students,
                    description = Subject.description,
                    nb_examens = Subject.nb_exams,
                    nb_examens_continue = Subject.nb_exams_continue,
                    success_rate = Subject.success_rate
    });
            }
            
            return Ok(result);
        }
        [HttpGet(template: "{id}")]
        [ProducesResponseType(typeof(Subject), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetSubjectAsync(int id)
        {
            var Subjects = await _repoSubject.GetByKeyAsync(id);
            return Ok(Subjects);
        }
        [HttpGet(template: "{id}/TeachingUnit")]
        [ProducesResponseType(typeof(IEnumerable<DTOCreateTeachingUnit>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetSubjectAdressAsync(int id)
        {
            IEnumerable<TeachingUnit> teachingUnits = (IEnumerable<TeachingUnit>)await _repoSubject.GetRelatedTeachingUnit(id);
           
           
            List<DTOCreateTeachingUnit> result = new ();
            foreach(var teachingUnit in teachingUnits)
            {
                result.Add(new DTOCreateTeachingUnit()
                {
                    date_start = teachingUnit.date_start,
                    date_end = teachingUnit.date_end,
                    description = teachingUnit.description,
                    
                });
            }
            
            return Ok(result);
        }
        
        [HttpPost()]
        [ProducesResponseType(typeof(DTOOnlySubject), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> SubjectCreateAsync(DTOCreateSubject? Subject)
        {
            if (Subject == null)
            {
                return Problem("Subject is null");
            }
            Subject newSubject = new Subject()
            {
                Id_category = Subject.Id_category,
                max_students = Subject.max_students,
                description = Subject.description,
                nb_exams = Subject.nb_examens,
                nb_exams_continue = Subject.nb_examens_continue,
                success_rate = Subject.success_rate
            };

            var createdSubject = await _repoSubject.CreateElementAsync(newSubject);
            return Ok(createdSubject);
        }
        [HttpPut()]
        [ProducesResponseType(typeof(DTOOnlySubject), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> UpdateAsync(DTOUpdateSubject Subject)
        {
            if (Subject == null)
            {
                return Problem("Subject is null");
            }
            
            var modifiedsSubject = await _repoSubject.GetByKeyAsync(Subject.id);
            
            if (modifiedsSubject == null)
                return Problem("Does not exist");

            modifiedsSubject.Id_category = Subject.Id_category;
            modifiedsSubject.max_students = Subject.max_students;
            modifiedsSubject.description = Subject.description;
            modifiedsSubject.nb_exams = Subject.nb_examens;
            modifiedsSubject.nb_exams_continue = Subject.nb_examens_continue;
            modifiedsSubject.success_rate = Subject.success_rate;

            var toReturn = await _repoSubject.UpdateElementAsync(modifiedsSubject);
            
            return Ok(toReturn);
        }
        [HttpDelete()]
        [ProducesResponseType(typeof(Subject), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> DeleteSubjectAsync(Subject Subject)
        {
            if (Subject == null)
                return Problem("The id");

            var toReturn = await _repoSubject.DeleteElementAsync(Subject);
            
            return Ok(toReturn);
        }

    }
}