using Microsoft.AspNetCore.Mvc;
using Service.Contract;
using simplonUniv.Dtos.Room;
using simplonUniv.Entities;


namespace simplonUniv.Controler
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExamController : ControllerBase
    {
        /// <summary>
        /// Le service de gestion des Exam
        /// </summary>
        private readonly IExamService _serviceExam;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExamService"/> class.
        /// </summary>
        /// <param name="serviceExam">The service Exam.</param>
        public ExamController(IExamService serviceExam)
        {
            _serviceExam = serviceExam;
        }

        // GET: api/Exam
        /// <summary>
        /// Ressource pour récupérer la liste des Exam
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        [ProducesResponseType(typeof(IEnumerable<DTOOnlyExam>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> ExamListAsync()
        {
            var exams = await _serviceExam.GetAllAsync();
            return Ok(exams);
        }
        // GET: api/Exam
        /// <summary>
        /// Ressource pour récupérer un Exam
        /// </summary>
        /// <returns></returns>
        [HttpGet(template: "{id}")]
        [ProducesResponseType(typeof(Exam), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetExamAsync(int id)
        {
            var exams = await _serviceExam.GetByKeyAsync(id);
            return Ok(exams);
        }
        // GET: api/Exam
        /// <summary>
        /// Ressource pour crée un Exam
        /// </summary>
        /// <returns></returns>
        [HttpPost()]
        [ProducesResponseType(typeof(DTOOnlyExam), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> ExamCreateAsync(DTOCreateExam exam)
        {
            var createdExam = await _serviceExam.CreateElementAsync(exam);
            return Ok(createdExam);
        }
        // GET: api/Exam
        /// <summary>
        /// Ressource pour modifier un Exam
        /// </summary>
        /// <returns></returns>
        [HttpPut()]
        [ProducesResponseType(typeof(DTOOnlyExam), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> UpdateAsync(DTOOnlyExam exam)
        {
            var toReturn = await _serviceExam.UpdateElement(exam);
            return Ok(toReturn);
        }
        // GET: api/Exam
        /// <summary>
        /// Ressource pour supprimer un Exam
        /// </summary>
        /// <returns></returns>
        [HttpDelete()]
        [ProducesResponseType(typeof(Exam), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> DeleteExamAsync(Exam exam)
        {
            var toReturn = await _serviceExam.DeleteElement(exam);
            
            return Ok(toReturn);
        }

    }
}