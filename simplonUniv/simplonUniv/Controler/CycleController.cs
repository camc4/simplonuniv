using Microsoft.AspNetCore.Mvc;
using Repository.Contract;
using simplonUniv.Dtos.Cycle;
using simplonUniv.Entities;


namespace simplonUniv.Controler
{
    [Route("api/[controller]")]
    [ApiController]
    public class CycleController : ControllerBase
    {
        /// <summary>
        /// Le repository de gestion des Cycle
        /// </summary>
        private readonly IRepoCycle _repoCycle;

        /// <summary>
        /// Initializes a new instance of the <see cref="CycleController"/> class.
        /// </summary>
        /// <param name="repoCycle">The repository Cycle.</param>
        public CycleController(IRepoCycle repoCycle)
        {
            _repoCycle = repoCycle;
        }

        // GET: api/Cycle
        /// <summary>
        /// Ressource pour récupérer la liste des Cycle
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        [ProducesResponseType(typeof(IEnumerable<DTOOnlyCycle>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> CycleListAsync()
        {
            var Cycles = await _repoCycle.GetAllAsync();
            List<DTOOnlyCycle> result = new ();
            foreach(var Cycle in Cycles)
            {
                result.Add(new DTOOnlyCycle()
                {
                    id = Cycle.id,
                    description = Cycle.description,
                    date_end = Cycle.date_end,
                    date_start = Cycle.date_start

                });
            }
            
            return Ok(result);
        }
        [HttpGet(template: "{id}")]
        [ProducesResponseType(typeof(Cycle), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetCycleAsync(int id)
        {
            var Cycles = await _repoCycle.GetByKeyAsync(id);
            return Ok(Cycles);
        }
        [HttpPost()]
        [ProducesResponseType(typeof(DTOOnlyCycle), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> CycleCreateAsync(DTOCreateCycle? Cycle)
        {
            if (Cycle == null)
            {
                return Problem("Cycle not found");
            }
            Cycle newCycle = new Cycle()
            {
                description = Cycle.description,
                date_end = Cycle.date_end,
                date_start = Cycle.date_start
            };

            var createdCycle = await _repoCycle.CreateElementAsync(newCycle);
            return Ok(createdCycle);
        }
        [HttpPut()]
        [ProducesResponseType(typeof(DTOOnlyCycle), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> UpdateAsync(DTOOnlyCycle Cycle)
        {
            if (Cycle == null)
            {
                return Problem("Cycle is null");
            }
            
            var modifiedsCycle = await _repoCycle.GetByKeyAsync(Cycle.id);
            
            if (modifiedsCycle == null)
                return Problem("Does not exist");

            modifiedsCycle.description = Cycle.description;
            modifiedsCycle.date_end = Cycle.date_end;
            modifiedsCycle.date_start = Cycle.date_start;
            
            var toReturn = await _repoCycle.UpdateElementAsync(modifiedsCycle);
            
            return Ok(toReturn);
        }
        [HttpDelete()]
        [ProducesResponseType(typeof(Cycle), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> DeleteCycleAsync(Cycle Cycle)
        {
            if (Cycle == null)
                return Problem("The id");

            var toReturn = await _repoCycle.DeleteElementAsync(Cycle);
            
            return Ok(toReturn);
        }

    }
}