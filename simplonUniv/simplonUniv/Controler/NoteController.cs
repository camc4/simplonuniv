using Microsoft.AspNetCore.Mvc;
using Repository.Contract;
using simplonUniv.Dtos.Room;
using simplonUniv.Entities;


namespace simplonUniv.Controler
{
    [Route("api/[controller]")]
    [ApiController]
    public class NoteController : ControllerBase
    {
        /// <summary>
        /// Le repository de gestion des Note
        /// </summary>
        private readonly IRepoNote _repoNote;

        /// <summary>
        /// Initializes a new instance of the <see cref="NoteController"/> class.
        /// </summary>
        /// <param name="repoNote">The repository Note.</param>
        public NoteController(IRepoNote repoNote)
        {
            _repoNote = repoNote;
        }

        // GET: api/Note
        /// <summary>
        /// Ressource pour récupérer la liste des Note
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        [ProducesResponseType(typeof(IEnumerable<DTOOnlyNote>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> NoteListAsync()
        {
            var Notes = await _repoNote.GetAllAsync();
            List<DTOOnlyNote> result = new ();
            foreach(var Note in Notes)
            {
                result.Add(new DTOOnlyNote()
                {
                    id = Note.id,
                    school_mark = Note.school_mark,
                    score_max = Note.score_max,
                    is_valided = Note.is_valided
                });
            }
            
            return Ok(result);
        }
        [HttpGet(template: "{id}")]
        [ProducesResponseType(typeof(Note), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetNoteAsync(int id)
        {
            var Notes = await _repoNote.GetByKeyAsync(id);
            return Ok(Notes);
        }
        [HttpPost()]
        [ProducesResponseType(typeof(DTOOnlyNote), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> NoteCreateAsync(DTOCreateNote? Note)
        {
            if (Note == null)
            {
                return Problem("Note not found");
            }
            Note newNote = new Note()
            {
                school_mark = Note.school_mark,
                score_max = Note.score_max,
                is_valided = Note.is_valided,
                Id_Student = Note.IdStudent,
                Id_Exam = Note.IdExam
            };

            var createdNote = await _repoNote.CreateElementAsync(newNote);
            return Ok(createdNote);
        }
        [HttpPut()]
        [ProducesResponseType(typeof(DTOOnlyNote), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> UpdateAsync(DTOOnlyNote Note)
        {
            if (Note == null)
            {
                return Problem("Note is null");
            }
            
            var modifiedsNote = await _repoNote.GetByKeyAsync(Note.id);
            
            if (modifiedsNote == null)
                return Problem("Does not exist");

            modifiedsNote.school_mark = Note.school_mark;
            modifiedsNote.score_max = Note.score_max;
            modifiedsNote.is_valided = Note.is_valided;

            var toReturn = await _repoNote.UpdateElementAsync(modifiedsNote);
            
            return Ok(toReturn);
        }
        [HttpDelete()]
        [ProducesResponseType(typeof(Note), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> DeleteNoteAsync(Note Note)
        {
            if (Note == null)
                return Problem("The id");

            var toReturn = await _repoNote.DeleteElementAsync(Note);
            
            return Ok(toReturn);
        }

    }
}