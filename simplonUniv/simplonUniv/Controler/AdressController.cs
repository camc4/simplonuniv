using Service.Contract;

namespace simplonUniv.Controler;
using Microsoft.AspNetCore.Mvc;
using Repository.Contract;
using simplonUniv.Dtos.Adress;
using simplonUniv.Dtos.Student;
using simplonUniv.Entities;

[Route("api/[controller]")]
[ApiController]
public class AdressController : ControllerBase
{
    /// <summary>
    /// Le repository de gestion des student
    /// </summary>
    private readonly IRepoStudent _repoStudent;
    private readonly IAdressService _adressService;

    /// <summary>
    /// Initializes a new instance of the <see cref="StudentController"/> class.
    /// </summary>
    /// <param name="repoStudent">The repository student.</param>
    public AdressController(IRepoStudent repoStudent,IAdressService _serviceAdress)
    {
        _repoStudent = repoStudent;
        _adressService = _serviceAdress;
    }

    // GET: api/Student
    /// <summary>
    /// Ressource pour récupérer la liste des Student
    /// </summary>
    /// <returns></returns>
    [HttpGet()]
    [ProducesResponseType(typeof(IEnumerable<DTOCreateAdress>), 200)]
    [ProducesResponseType(typeof(StatusCodeResult), 500)]
    [ProducesResponseType(typeof(StatusCodeResult), 400)]
    public async Task<ActionResult> AdressListAsync()
    {
        var adresses = await _adressService.GetAllAsync();
        
        return Ok(adresses);
    }
    [HttpGet(template: "{id}")]
    [ProducesResponseType(typeof(Adress), 200)]
    [ProducesResponseType(typeof(StatusCodeResult), 500)]
    [ProducesResponseType(typeof(StatusCodeResult), 400)]
    public async Task<ActionResult> GetAdressAsync(int id)
    {
        var adress = await _adressService.GetByKeyAsync(id);
        return Ok(adress);
    }
    [HttpGet(template: "{id}/student")]
    [ProducesResponseType(typeof(IEnumerable<DTOCreateStudent>), 200)]
    [ProducesResponseType(typeof(StatusCodeResult), 500)]
    [ProducesResponseType(typeof(StatusCodeResult), 400)]
    public async Task<ActionResult> GetAdressStudentAsync(int id)
    {
        var students = await _adressService.GetRelatedStudent(id);
        return Ok(students);
    }
    [HttpPost(template: "{id}/student")]
    [ProducesResponseType(typeof(IEnumerable<DTOOnlyStudent>), 200)]
    [ProducesResponseType(typeof(StatusCodeResult), 500)]
    [ProducesResponseType(typeof(StatusCodeResult), 400)]
    public async Task<ActionResult> CreateStudentAdressAsync(int id,DTOCreateStudent student)
    {
        IEnumerable<DTOOnlyStudent> students = await _adressService.CreateAndAddToAdress(id,student);
        
        return Ok(students);
    }
    [HttpDelete(template: "{id}/student/{id_student}")]
    [ProducesResponseType(typeof(IEnumerable<DTOOnlyStudent>), 200)]
    [ProducesResponseType(typeof(StatusCodeResult), 500)]
    [ProducesResponseType(typeof(StatusCodeResult), 400)]
    public async Task<ActionResult> DeleteStudentAdressAsync(int id,int id_student)
    {
        IEnumerable<DTOOnlyStudent> students = await _adressService.DeleteStudentOfAdress(id,id_student);
        
        return Ok(students);
    }
    [HttpPost()]
    [ProducesResponseType(typeof(DTOCreateAdress), 200)]
    [ProducesResponseType(typeof(StatusCodeResult), 500)]
    [ProducesResponseType(typeof(StatusCodeResult), 400)]
    public async Task<ActionResult> AdressCreateAsync(DTOCreateAdress adress)
    {
        var createdAdress = await _adressService.CreateElementAsync(adress);
        return Ok(createdAdress);
    }
    [HttpPut()]
    [ProducesResponseType(typeof(DTOOnlyAdress), 200)]
    [ProducesResponseType(typeof(StatusCodeResult), 500)]
    [ProducesResponseType(typeof(StatusCodeResult), 400)]
    public async Task<ActionResult> UpdateAsync(DTOOnlyAdress adress)
    {
        var toReturn = await _adressService.UpdateElement(adress);
        
        return Ok(toReturn);
    }
    [HttpDelete()]
    [ProducesResponseType(typeof(DTOOnlyAdress), 200)]
    [ProducesResponseType(typeof(StatusCodeResult), 500)]
    [ProducesResponseType(typeof(StatusCodeResult), 400)]
    public async Task<ActionResult> DeleteStudentAsync(Adress adress)
    {
        var toReturn = await _adressService.DeleteElement(adress);
        
        return Ok(toReturn);
    }

}
