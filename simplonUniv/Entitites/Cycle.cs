﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace simplonUniv.Entities;

[Table("Cycle")]
public partial class Cycle
{
    [Key]
    [Column(TypeName = "int(11)")]
    public int id { get; set; }

    [StringLength(255)]
    public string? description { get; set; }

    public DateOnly date_start { get; set; }

    public DateOnly date_end { get; set; }

    [InverseProperty("Id_CycleNavigation")]
    public virtual ICollection<TeachingUnit> TeachingUnits { get; } = new List<TeachingUnit>();
}
