﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace simplonUniv.Entities;

[Table("CategorySubject")]
public partial class CategorySubject
{
    [Key]
    [Column(TypeName = "int(11)")]
    public int id { get; set; }

    [StringLength(50)]
    public string name { get; set; } = null!;

    [StringLength(50)]
    public string? description { get; set; }

    [InverseProperty("Id_categoryNavigation")]
    public virtual ICollection<Subject> Subjects { get; } = new List<Subject>();
}
