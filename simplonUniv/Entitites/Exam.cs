﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace simplonUniv.Entities;

[Table("Exam")]
[Index("Id_teachingUnit", Name = "Id_teachingUnit")]
public partial class Exam
{
    [Key]
    [Column(TypeName = "int(11)")]
    public int id { get; set; }

    public DateOnly exam_date { get; set; }

    [Column(TypeName = "enum('oral','tp','group work','final')")]
    public string type { get; set; } = null!;

    [Column(TypeName = "int(11)")]
    public int Id_teachingUnit { get; set; }

    [ForeignKey("Id_teachingUnit")]
    [InverseProperty("Exams")]
    public virtual TeachingUnit Id_teachingUnitNavigation { get; set; } = null!;

    [InverseProperty("Id_ExamNavigation")]
    public virtual ICollection<Note> Notes { get; } = new List<Note>();
}
