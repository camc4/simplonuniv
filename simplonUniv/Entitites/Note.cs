﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace simplonUniv.Entities;

[Table("Note")]
[Index("Id_Exam", Name = "Id_Exam")]
[Index("Id_Student", Name = "Id_Student")]
public partial class Note
{
    [Key]
    [Column(TypeName = "int(11)")]
    public int id { get; set; }

    [Column(TypeName = "int(11)")]
    public int school_mark { get; set; }

    [Column(TypeName = "int(11)")]
    public int score_max { get; set; }

    public bool? is_valided { get; set; }

    [Column(TypeName = "int(11)")]
    public int Id_Student { get; set; }

    [Column(TypeName = "int(11)")]
    public int Id_Exam { get; set; }

    [ForeignKey("Id_Exam")]
    [InverseProperty("Notes")]
    public virtual Exam Id_ExamNavigation { get; set; } = null!;

    [ForeignKey("Id_Student")]
    [InverseProperty("Notes")]
    public virtual Student Id_StudentNavigation { get; set; } = null!;
}
