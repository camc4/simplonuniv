﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace simplonUniv.Entities;

[Table("Room")]
public partial class Room
{
    [Key]
    [Column(TypeName = "int(11)")]
    public int id { get; set; }

    [StringLength(50)]
    public string name { get; set; } = null!;

    [InverseProperty("Id_RoomNavigation")]
    public virtual ICollection<HalfDay> HalfDays { get; } = new List<HalfDay>();
}
