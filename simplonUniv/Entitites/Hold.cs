﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace simplonUniv.Entities;

[PrimaryKey("Id_Student", "Id_AcademicLevel")]
[Table("Hold")]
[Index("Id_AcademicLevel", Name = "Id_AcademicLevel")]
public partial class Hold
{
    [Key]
    [Column(TypeName = "int(11)")]
    public int Id_Student { get; set; }

    [Key]
    [Column(TypeName = "int(11)")]
    public int Id_AcademicLevel { get; set; }

    [Column(TypeName = "int(11)")]
    public int years_academic { get; set; }

    [ForeignKey("Id_AcademicLevel")]
    [InverseProperty("Holds")]
    public virtual AcademicLevel Id_AcademicLevelNavigation { get; set; } = null!;

    [ForeignKey("Id_Student")]
    [InverseProperty("Holds")]
    public virtual Student Id_StudentNavigation { get; set; } = null!;
}
