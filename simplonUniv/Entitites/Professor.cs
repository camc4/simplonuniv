﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace simplonUniv.Entities;

[Table("Professor")]
public partial class Professor
{
    [Key]
    [Column(TypeName = "int(11)")]
    public int id { get; set; }

    [StringLength(50)]
    public string first_name { get; set; } = null!;

    [StringLength(50)]
    public string last_name { get; set; } = null!;

    [StringLength(50)]
    public string? email { get; set; }

    [InverseProperty("Id_ProfessorNavigation")]
    public virtual ICollection<Course> Courses { get; } = new List<Course>();
}
