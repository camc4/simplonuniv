﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace simplonUniv.Entities;

[Table("Student")]
public partial class Student
{
    [Key]
    [Column(TypeName = "int(11)")]
    public int id { get; set; }

    [StringLength(50)]
    public string last_name { get; set; } = null!;

    [StringLength(50)]
    public string? email { get; set; }

    [StringLength(50)]
    public string? tel { get; set; }

    [StringLength(50)]
    public string first_name { get; set; } = null!;

    public DateOnly? date_birth { get; set; }

    public DateOnly date_inscription { get; set; }

    [InverseProperty("Id_StudentNavigation")]
    public virtual ICollection<HalfDay> HalfDays { get; } = new List<HalfDay>();

    [InverseProperty("Id_StudentNavigation")]
    public virtual ICollection<Hold> Holds { get; } = new List<Hold>();

    [InverseProperty("Id_StudentNavigation")]
    public virtual ICollection<Note> Notes { get; } = new List<Note>();

    [ForeignKey("Id_Student")]
    [InverseProperty("Id_Students")]
    public virtual ICollection<Adress> Id_Adresses { get; } = new List<Adress>();

    [ForeignKey("Id_Student")]
    [InverseProperty("Id_Students")]
    public virtual ICollection<TeachingUnit> Id_TeachingUnits { get; } = new List<TeachingUnit>();
}
