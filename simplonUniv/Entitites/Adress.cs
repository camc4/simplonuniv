﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace simplonUniv.Entities;

[Table("Adress")]
public partial class Adress
{
    [Key]
    [Column(TypeName = "int(11)")]
    public int id { get; set; }

    [StringLength(50)]
    public string city { get; set; } = null!;

    [StringLength(50)]
    public string country { get; set; } = null!;

    [StringLength(50)]
    public string street { get; set; } = null!;

    [StringLength(50)]
    public string? zipcode { get; set; }

    [StringLength(50)]
    public string number { get; set; } = null!;

    [ForeignKey("Id_Adress")]
    [InverseProperty("Id_Adresses")]
    public virtual ICollection<Student> Id_Students { get; } = new List<Student>();
}
