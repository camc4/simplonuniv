﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace simplonUniv.Entities;

[Table("Course")]
[Index("Id_Professor", Name = "Id_Professor")]
[Index("Id_teachingUnit", Name = "Id_teachingUnit")]
public partial class Course
{
    [Key]
    [Column(TypeName = "int(11)")]
    public int id { get; set; }

    public DateOnly period_start { get; set; }

    public DateOnly period_end { get; set; }

    [Column(TypeName = "int(11)")]
    public int Id_teachingUnit { get; set; }

    [Column(TypeName = "int(11)")]
    public int Id_Professor { get; set; }

    [InverseProperty("Id_CourseNavigation")]
    public virtual ICollection<HalfDay> HalfDays { get; } = new List<HalfDay>();

    [ForeignKey("Id_Professor")]
    [InverseProperty("Courses")]
    public virtual Professor Id_ProfessorNavigation { get; set; } = null!;

    [ForeignKey("Id_teachingUnit")]
    [InverseProperty("Courses")]
    public virtual TeachingUnit Id_teachingUnitNavigation { get; set; } = null!;
}
