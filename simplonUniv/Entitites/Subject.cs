﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace simplonUniv.Entities;

[Table("Subject")]
[Index("Id_category", Name = "Id_category")]
public partial class Subject
{
    [Key]
    [Column(TypeName = "int(11)")]
    public int id { get; set; }

    [Column(TypeName = "int(11)")]
    public int max_students { get; set; }

    [StringLength(255)]
    public string? description { get; set; }

    [Column(TypeName = "int(11)")]
    public int nb_exams { get; set; }

    [Column(TypeName = "int(11)")]
    public int nb_exams_continue { get; set; }

    [Precision(15, 2)]
    public decimal success_rate { get; set; }

    [Column(TypeName = "int(11)")]
    public int Id_category { get; set; }

    [ForeignKey("Id_category")]
    [InverseProperty("Subjects")]
    public virtual CategorySubject Id_categoryNavigation { get; set; } = null!;

    [InverseProperty("Id_SubjectNavigation")]
    public virtual ICollection<TeachingUnit> TeachingUnits { get; } = new List<TeachingUnit>();
}
