﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace simplonUniv.Entities;

[Table("HalfDay")]
[Index("Id_Course", Name = "Id_Course")]
[Index("Id_Room", Name = "Id_Room")]
[Index("Id_Student", Name = "Id_Student")]
public partial class HalfDay
{
    [Key]
    [Column(TypeName = "int(11)")]
    public int id { get; set; }

    public bool has_signed { get; set; }

    [Column(TypeName = "datetime")]
    public DateTime date { get; set; }

    [Column(TypeName = "int(11)")]
    public int Id_Room { get; set; }

    [Column(TypeName = "int(11)")]
    public int Id_Student { get; set; }

    [Column(TypeName = "int(11)")]
    public int Id_Course { get; set; }

    [ForeignKey("Id_Course")]
    [InverseProperty("HalfDays")]
    public virtual Course Id_CourseNavigation { get; set; } = null!;

    [ForeignKey("Id_Room")]
    [InverseProperty("HalfDays")]
    public virtual Room Id_RoomNavigation { get; set; } = null!;

    [ForeignKey("Id_Student")]
    [InverseProperty("HalfDays")]
    public virtual Student Id_StudentNavigation { get; set; } = null!;
}
