﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace simplonUniv.Entities;

[Table("TeachingUnit")]
[Index("Id_Cycle", Name = "Id_Cycle")]
[Index("Id_Subject", Name = "Id_Subject")]
public partial class TeachingUnit
{
    [Key]
    [Column(TypeName = "int(11)")]
    public int id { get; set; }

    public DateOnly date_start { get; set; }

    public DateOnly date_end { get; set; }

    [StringLength(255)]
    public string? description { get; set; }

    [Column(TypeName = "int(11)")]
    public int Id_Subject { get; set; }

    [Column(TypeName = "int(11)")]
    public int Id_Cycle { get; set; }

    [InverseProperty("Id_teachingUnitNavigation")]
    public virtual ICollection<Course> Courses { get; } = new List<Course>();

    [InverseProperty("Id_teachingUnitNavigation")]
    public virtual ICollection<Exam> Exams { get; } = new List<Exam>();

    [ForeignKey("Id_Cycle")]
    [InverseProperty("TeachingUnits")]
    public virtual Cycle Id_CycleNavigation { get; set; } = null!;

    [ForeignKey("Id_Subject")]
    [InverseProperty("TeachingUnits")]
    public virtual Subject Id_SubjectNavigation { get; set; } = null!;

    [ForeignKey("Id_TeachingUnit")]
    [InverseProperty("Id_TeachingUnits")]
    public virtual ICollection<Student> Id_Students { get; } = new List<Student>();
}
