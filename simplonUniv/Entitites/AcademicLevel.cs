﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace simplonUniv.Entities;

[Table("AcademicLevel")]
public partial class AcademicLevel
{
    [Key]
    [Column(TypeName = "int(11)")]
    public int id { get; set; }

    [Column(TypeName = "enum('l1','l2','l3','l4','l5')")]
    public string level { get; set; } = null!;

    [InverseProperty("Id_AcademicLevelNavigation")]
    public virtual ICollection<Hold> Holds { get; } = new List<Hold>();
}
