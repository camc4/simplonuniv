﻿using Contract;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Repository;
using Repository.Contract;
using Service;
using Service.Contract;
using simplonUniv;

namespace Ioc;

public static class IoCApp
{
    /// <summary>
    /// Configuration de l'injection des services du Web API RestFul
    /// </summary>
    /// <param name="services"></param>
    public static IServiceCollection ConfigureInjectionDependencyRepo(this IServiceCollection services)
    {
        services.AddScoped<IRepoStudent, RepoStudent>();
        services.AddScoped<IRepoAdress, RepoAdress>();
        services.AddScoped<IRepoProfessor, RepoProfessor>();
        services.AddScoped<IRepoTeachingUnit, RepoTeachingUnit>();
        services.AddScoped<IRepoSubject, RepoSubject>();
        services.AddScoped<IRepoCategorySubject, RepoCategorySubject>();
        services.AddScoped<IRepoCycle, RepoCycle>();
        services.AddScoped<IRepoAcademicLevel, RepoAcademicLevel>();
        services.AddScoped<IRepoHalfDay, RepoHalfDay>();
        services.AddScoped<IRepoNote, RepoNote>();
        services.AddScoped<IRepoCourse, RepoCourse>();
        services.AddScoped<IRepoExam, RepoExam>();

        return services;
    }
    public static IServiceCollection ConfigureInjectionDependencyService(this IServiceCollection services)
    {
        services.AddScoped<IStudentService,StudentService>();
        services.AddScoped<IProfessorService,ProfessorService>();
        services.AddScoped<IAdressService,AdressService>();
        services.AddScoped<IAcademicLevelService,AcademicLevelService>();
        services.AddScoped<IHalfDayService,HalfDayService>();
        services.AddScoped<IExamService,ExamService>();
        services.AddScoped<ICourseService,CourseService>();
        return services;
    }

    /// <summary>
    /// Configuration de la connexion de la base de données
    /// </summary>
    /// <param name="services"></param>
    public static IServiceCollection ConfigureDBContext(this IServiceCollection services, IConfiguration configuration)
    {
        var connectionString = configuration.GetConnectionString("BddConnection");
        //var connectionString = "Server=localhost;User=root;Password=root;Database=simplonUniv;port=3306";
        
        services.AddDbContext<IMyDbContext, MyDbContext>(options => options.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString),
                b => b.MigrationsAssembly("SimplonUniv.App"))
            .LogTo(Console.WriteLine, LogLevel.Information)
            .EnableSensitiveDataLogging()
            .EnableDetailedErrors());
    
        return services;
    }

}
