
using simplonUniv.Entities;

namespace Repository.Contract
{
    public interface IRepoSubject : IGenericRepo<Subject>
    {
        Task<object> GetRelatedTeachingUnit(int id);
    }
}