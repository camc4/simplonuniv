
using simplonUniv.Entities;

namespace Repository.Contract
{
    public interface IRepoExam : IGenericRepo<Exam>
    {
    }
}