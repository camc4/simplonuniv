
using simplonUniv.Entities;

namespace Repository.Contract
{
    public interface IRepoCategorySubject : IGenericRepo<CategorySubject>
    {
        Task<object> GetRelatedSubject(int id);
    }
}