
using simplonUniv.Entities;

namespace Repository.Contract
{
    public interface IRepoNote : IGenericRepo<Note>
    {
    }
}