
using simplonUniv.Entities;

namespace Repository.Contract
{
    public interface IRepoCycle : IGenericRepo<Cycle>
    {
    }
}