
using simplonUniv.Entities;

namespace Repository.Contract
{
    public interface IRepoProfessor : IGenericRepo<Professor>
    {
        Task<object> GetRelatedCourse(int id);
        void AssignToCourse(Professor professor, Course course);
    }
}