
using simplonUniv.Entities;

namespace Repository.Contract
{
    public interface IRepoAcademicLevel : IGenericRepo<AcademicLevel>
    {
    }
}