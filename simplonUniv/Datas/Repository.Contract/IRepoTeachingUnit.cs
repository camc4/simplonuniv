
using simplonUniv.Entities;

namespace Repository.Contract
{
    public interface IRepoTeachingUnit : IGenericRepo<TeachingUnit>
    {
        Task<object?> GetAllAndRelatedStudent();
        object getRelatedSubject(TeachingUnit teachingUnit);
    }
}