
using simplonUniv.Entities;

namespace Repository.Contract
{
    public interface IRepoStudent : IGenericRepo<Student>
    {
        Task<IEnumerable<Adress>> GetRelatedAdress(int id);
        Task registerToTeachingUnit(TeachingUnit teachingUnit, Student student);
        Task<IEnumerable<object>> GetRelatedTeachingUnit(int id);
    }
}