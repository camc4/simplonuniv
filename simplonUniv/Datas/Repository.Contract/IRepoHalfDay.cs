
using simplonUniv.Entities;

namespace Repository.Contract
{
    public interface IRepoHalfDay : IGenericRepo<HalfDay>
    {
    }
}