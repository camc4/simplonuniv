
using simplonUniv.Entities;

namespace Repository.Contract
{
    public interface IRepoRoom : IGenericRepo<Room>
    {
    }
}