
using simplonUniv.Entities;

namespace Repository.Contract
{
    public interface IRepoAdress : IGenericRepo<Adress>
    {
        Task<Adress> AddToStudent(Student Student,Adress adress);
        Task<object> AddToAdress(Adress adress, Student newStudent);
        Task<object> GetRelatedStudent(int id);
        void RemoveRelatedStudent(int idAdress, int idStudent);
    }
}