
using simplonUniv.Entities;

namespace Repository.Contract
{
    public interface IRepoCourse : IGenericRepo<Course>
    {
    }
}