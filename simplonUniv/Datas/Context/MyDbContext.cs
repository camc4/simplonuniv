﻿using System;
using System.Collections.Generic;
using Contract;
using Microsoft.EntityFrameworkCore;
using simplonUniv.Entities;

namespace simplonUniv;

public partial class MyDbContext : DbContext, IMyDbContext
{
    public MyDbContext()
    {
    }

    public MyDbContext(DbContextOptions<MyDbContext> options)
        : base(options)
    {
    }

    // public virtual DbSet<AcademicLevel> AcademicLevels { get; set; }
    //
    // public virtual DbSet<Adress> Adresses { get; set; }
    //
    // public virtual DbSet<CategorySubject> CategorySubjects { get; set; }
    //
    // public virtual DbSet<Course> Courses { get; set; }
    //
    // public virtual DbSet<Cycle> Cycles { get; set; }
    //
    // public virtual DbSet<Exam> Exams { get; set; }
    //
    // public virtual DbSet<HalfDay> HalfDays { get; set; }
    //
    // public virtual DbSet<Hold> Holds { get; set; }
    //
    // public virtual DbSet<Note> Notes { get; set; }
    //
    // public virtual DbSet<Professor> Professors { get; set; }
    //
    // public virtual DbSet<Room> Rooms { get; set; }
    //
    // public virtual DbSet<Student> Students { get; set; }
    //
    // public virtual DbSet<Subject> Subjects { get; set; }
    //
    // public virtual DbSet<TeachingUnit> TeachingUnits { get; set; }

    //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
      //  => optionsBuilder.UseMySql("server=localhost;user=root;password=root;database=simplonUniv;port=3306", Microsoft.EntityFrameworkCore.ServerVersion.Parse("10.6.12-mariadb"));

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder
            .UseCollation("utf8mb4_general_ci")
            .HasCharSet("utf8mb4");

        modelBuilder.Entity<AcademicLevel>(entity =>
        {
            entity.HasKey(e => e.id).HasName("PRIMARY");
        });

        modelBuilder.Entity<Adress>(entity =>
        {
            entity.HasKey(e => e.id).HasName("PRIMARY");
        });

        modelBuilder.Entity<CategorySubject>(entity =>
        {
            entity.HasKey(e => e.id).HasName("PRIMARY");
        });

        modelBuilder.Entity<Course>(entity =>
        {
            entity.HasKey(e => e.id).HasName("PRIMARY");

            entity.HasOne(d => d.Id_ProfessorNavigation).WithMany(p => p.Courses)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("Course_ibfk_2");

            entity.HasOne(d => d.Id_teachingUnitNavigation).WithMany(p => p.Courses)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("Course_ibfk_1");
        });

        modelBuilder.Entity<Cycle>(entity =>
        {
            entity.HasKey(e => e.id).HasName("PRIMARY");
        });

        modelBuilder.Entity<Exam>(entity =>
        {
            entity.HasKey(e => e.id).HasName("PRIMARY");

            entity.HasOne(d => d.Id_teachingUnitNavigation).WithMany(p => p.Exams)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("Exam_ibfk_1");
        });

        modelBuilder.Entity<HalfDay>(entity =>
        {
            entity.HasKey(e => e.id).HasName("PRIMARY");

            entity.HasOne(d => d.Id_CourseNavigation).WithMany(p => p.HalfDays)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("HalfDay_ibfk_3");

            entity.HasOne(d => d.Id_RoomNavigation).WithMany(p => p.HalfDays)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("HalfDay_ibfk_1");

            entity.HasOne(d => d.Id_StudentNavigation).WithMany(p => p.HalfDays)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("HalfDay_ibfk_2");
        });

        modelBuilder.Entity<Hold>(entity =>
        {
            entity.HasKey(e => new { e.Id_Student, e.Id_AcademicLevel })
                .HasName("PRIMARY")
                .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

            entity.HasOne(d => d.Id_AcademicLevelNavigation).WithMany(p => p.Holds)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("Hold_ibfk_2");

            entity.HasOne(d => d.Id_StudentNavigation).WithMany(p => p.Holds)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("Hold_ibfk_1");
        });

        modelBuilder.Entity<Note>(entity =>
        {
            entity.HasKey(e => e.id).HasName("PRIMARY");

            entity.HasOne(d => d.Id_ExamNavigation).WithMany(p => p.Notes)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("Note_ibfk_2");

            entity.HasOne(d => d.Id_StudentNavigation).WithMany(p => p.Notes)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("Note_ibfk_1");
        });

        modelBuilder.Entity<Professor>(entity =>
        {
            entity.HasKey(e => e.id).HasName("PRIMARY");
        });

        modelBuilder.Entity<Room>(entity =>
        {
            entity.HasKey(e => e.id).HasName("PRIMARY");
        });

        modelBuilder.Entity<Student>(entity =>
        {
            entity.HasKey(e => e.id).HasName("PRIMARY");

            entity.HasMany(d => d.Id_Adresses).WithMany(p => p.Id_Students)
                .UsingEntity<Dictionary<string, object>>(
                    "Reside",
                    r => r.HasOne<Adress>().WithMany()
                        .HasForeignKey("Id_Adress")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("Reside_ibfk_2"),
                    l => l.HasOne<Student>().WithMany()
                        .HasForeignKey("Id_Student")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("Reside_ibfk_1"),
                    j =>
                    {
                        j.HasKey("Id_Student", "Id_Adress")
                            .HasName("PRIMARY")
                            .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });
                        j.ToTable("Reside");
                        j.HasIndex(new[] { "Id_Adress" }, "Id_Adress");
                        j.IndexerProperty<int>("Id_Student").HasColumnType("int(11)");
                        j.IndexerProperty<int>("Id_Adress").HasColumnType("int(11)");
                    });

            entity.HasMany(d => d.Id_TeachingUnits).WithMany(p => p.Id_Students)
                .UsingEntity<Dictionary<string, object>>(
                    "Inscription",
                    r => r.HasOne<TeachingUnit>().WithMany()
                        .HasForeignKey("Id_TeachingUnit")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("Inscription_ibfk_2"),
                    l => l.HasOne<Student>().WithMany()
                        .HasForeignKey("Id_Student")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("Inscription_ibfk_1"),
                    j =>
                    {
                        j.HasKey("Id_Student", "Id_TeachingUnit")
                            .HasName("PRIMARY")
                            .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });
                        j.ToTable("Inscription");
                        j.HasIndex(new[] { "Id_TeachingUnit" }, "Id_TeachingUnit");
                        j.IndexerProperty<int>("Id_Student").HasColumnType("int(11)");
                        j.IndexerProperty<int>("Id_TeachingUnit").HasColumnType("int(11)");
                    });
        });

        modelBuilder.Entity<Subject>(entity =>
        {
            entity.HasKey(e => e.id).HasName("PRIMARY");

            entity.HasOne(d => d.Id_categoryNavigation).WithMany(p => p.Subjects)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("Subject_ibfk_1");
        });

        modelBuilder.Entity<TeachingUnit>(entity =>
        {
            entity.HasKey(e => e.id).HasName("PRIMARY");

            entity.HasOne(d => d.Id_CycleNavigation).WithMany(p => p.TeachingUnits)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("TeachingUnit_ibfk_2");

            entity.HasOne(d => d.Id_SubjectNavigation).WithMany(p => p.TeachingUnits)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("TeachingUnit_ibfk_1");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    public DbSet<AcademicLevel> AcademicLevel { get; set; }
    public DbSet<Adress> Adress { get; set; }
    public DbSet<CategorySubject> CategorySubject { get; set; }
    public DbSet<Course> Course { get; set; }
    public DbSet<Cycle> Cycle { get; set; }
    public DbSet<Hold> Hold { get; set; }
    public DbSet<Exam> Exam { get; set; }
    public DbSet<HalfDay> HalfDay { get; set; }
    public DbSet<Note> Note { get; set; }
    public DbSet<Professor> Professor { get; set; }
    public DbSet<Room> Room { get; set; }
    public DbSet<Student> Student { get; set; }
    public DbSet<Subject> Subject { get; set; }
    public DbSet<TeachingUnit> TeachingUnit { get; set; }
}
