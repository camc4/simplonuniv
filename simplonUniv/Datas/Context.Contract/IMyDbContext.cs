using Microsoft.EntityFrameworkCore;
using simplonUniv.Entities;

namespace Contract;

public interface IMyDbContext : IDbContext
{

    DbSet<AcademicLevel> AcademicLevel { get; set; }

    DbSet<Adress> Adress { get; set; }

    DbSet<CategorySubject> CategorySubject { get; set; }
    
    DbSet<Course> Course { get; set; }
    
    DbSet<Cycle> Cycle { get; set; }
    
    DbSet<Hold> Hold { get; set; }
    
    DbSet<Exam> Exam { get; set; }
    
    DbSet<HalfDay> HalfDay { get; set; }
    
    DbSet<Note> Note { get; set; }
    
    DbSet<Professor> Professor { get; set; }
    
    DbSet<Room> Room { get; set; }
    
    DbSet<Student> Student { get; set; }
    
    DbSet<Subject> Subject { get; set; }
    
    DbSet<TeachingUnit> TeachingUnit { get; set; }
}
