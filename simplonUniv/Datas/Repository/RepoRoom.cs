using Contract;
using Microsoft.EntityFrameworkCore;
using Repository.Contract;
using simplonUniv.Entities;

namespace Repository
{
    public class RepoRoom : GenericRepository<Room>, IRepoRoom
    {

        public RepoRoom(IMyDbContext myContext): base(myContext)
        {
        }

    }
}

