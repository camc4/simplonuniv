using Contract;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Repository;
using Repository.Contract;
using simplonUniv.Dtos.Student;
using simplonUniv.Entities;

namespace Repository
{
    public class RepoAcademicLevel : GenericRepository<AcademicLevel>, IRepoAcademicLevel
    {

        public RepoAcademicLevel(IMyDbContext myContext): base(myContext)
        {
        }
        
    }
}