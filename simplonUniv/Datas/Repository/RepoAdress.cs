using Contract;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Repository;
using Repository.Contract;
using simplonUniv.Entities;

namespace Repository
{
    public class RepoAdress : GenericRepository<Adress>, IRepoAdress
    {

        public RepoAdress(IMyDbContext myContext): base(myContext)
        {
        }

        public async Task<Adress> AddToStudent(Student student,Adress newadress)
        {
            student.Id_Adresses.Add(newadress);
            _myContext.SaveChanges();
            return newadress;
        }

        public async Task<object> AddToAdress(Adress adress, Student newStudent)
        {
            adress.Id_Students.Add(newStudent);
            _myContext.SaveChanges();
            return newStudent;
        }

        public async Task<object> GetRelatedStudent(int id)
        {
            return _myContext.Adress.Where(e => e.id == id).Include(e => e.Id_Students).ToList();
        }

        public void RemoveRelatedStudent(int idAdress, int idStudent)
        {
            var student = _myContext.Student.Find(idStudent);
            var adress = _myContext.Adress.Find(idAdress).Id_Students.Remove(student);
        }
    }
}