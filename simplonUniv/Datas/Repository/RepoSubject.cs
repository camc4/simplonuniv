using Contract;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Repository;
using Repository.Contract;
using simplonUniv.Entities;

namespace Repository
{
    public class RepoSubject : GenericRepository<Subject>, IRepoSubject
    {

        public RepoSubject(IMyDbContext myContext): base(myContext)
        {
        }
        
        public async Task<object> GetRelatedTeachingUnit(int id)
        {
            var a = _myContext.Subject.Where(e => e.id == id).Include(e => e.TeachingUnits).First();
            return a.TeachingUnits;
            return _myContext.TeachingUnit.FromSqlRaw($"SELECT * FROM TeachingUnit WHERE TeachingUnit.Id_Subject = {id}").ToList();
        }
    }
}