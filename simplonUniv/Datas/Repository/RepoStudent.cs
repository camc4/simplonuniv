using Contract;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Repository;
using Repository.Contract;
using simplonUniv.Entities;

namespace Repository
{
    public class RepoStudent : GenericRepository<Student>, IRepoStudent
    {

        public RepoStudent(IMyDbContext myContext): base(myContext)
        {
        }

        public async Task<IEnumerable<Adress>> GetRelatedAdress(int id)
        {
            var a = _myContext.Student.Where(e => e.id == id).Include(e => e.Id_Adresses).ToList();
            if (a.Count == 0)
            {
                return Array.Empty<Adress>();
            }
            return a.First().Id_Adresses;
            // return _myContext.Adress.FromSqlRaw($"SELECT * FROM Adress WHERE Adress.id IN " +
            //                                     $"(SELECT habiter.Id_adress FROM habiter WHERE habiter.Id_student = {id})");
        }

        public async Task registerToTeachingUnit(TeachingUnit teachingUnit, Student student)
        {
            student.Id_TeachingUnits.Add(teachingUnit);
            _myContext.SaveChanges();
        }

        public async Task<IEnumerable<object>> GetRelatedTeachingUnit(int id)
        {
            var teachingUnits = _myContext.Student
                .Where(e => e.id == id)
                .Include(e => e.Id_TeachingUnits)
                .ThenInclude(e=>e.Exams)
                .ThenInclude(exam => exam.Notes.Where(e=>e.Id_Student==id))
                .First()
                .Id_TeachingUnits;

            
            
            return teachingUnits;
            
            
        }
    }
}