using Contract;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Repository;
using Repository.Contract;
using simplonUniv.Entities;

namespace Repository
{
    public class RepoHalfDay : GenericRepository<HalfDay>, IRepoHalfDay
    {

        public RepoHalfDay(IMyDbContext myContext): base(myContext)
        {
        }
        
    }
}