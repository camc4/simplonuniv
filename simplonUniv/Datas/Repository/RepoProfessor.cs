using Contract;
using Microsoft.EntityFrameworkCore;
using Repository.Contract;
using simplonUniv.Entities;

namespace Repository
{
    public class RepoProfessor : GenericRepository<Professor>, IRepoProfessor
    {

        public RepoProfessor(IMyDbContext myContext): base(myContext)
        {
        }
        
        public async Task<object> GetRelatedCourse(int id)
        {
            var a = _myContext.Professor.Where(e => e.id == id).Include(e => e.Courses).First();
            return a.Courses;
            return _myContext.Course.FromSqlRaw($"SELECT * FROM Course WHERE Course.Id_Professor = {id})").ToList();
        }

        public async void AssignToCourse(Professor professor, Course course)
        {
            professor.Courses.Add(course);
            _myContext.SaveChanges();
        }
    }
}

