using Contract;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Repository;
using Repository.Contract;
using simplonUniv.Dtos.Student;
using simplonUniv.Entities;

namespace Repository
{
    public class RepoTeachingUnit : GenericRepository<TeachingUnit>, IRepoTeachingUnit
    {

        public RepoTeachingUnit(IMyDbContext myContext): base(myContext)
        {
        }


        public async Task<object?> GetAllAndRelatedStudent()
        {
            return _myContext.TeachingUnit.
                Include(e => e.Id_Students).
                ToList();
        }

        public object getRelatedSubject(TeachingUnit teachingUnit)
        {
            return _myContext.TeachingUnit.Where(e => e.id == teachingUnit.id).Include(e => e.Id_Subject).First().Id_Subject;
            
        }
    }
}