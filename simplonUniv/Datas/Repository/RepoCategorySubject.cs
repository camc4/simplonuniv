using Contract;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Repository;
using Repository.Contract;
using simplonUniv.Entities;

namespace Repository
{
    public class RepoCategorySubject : GenericRepository<CategorySubject>, IRepoCategorySubject
    {

        public RepoCategorySubject(IMyDbContext myContext): base(myContext)
        {
        }

        public async Task<object> GetRelatedSubject(int id)
        {
            var a = _myContext.CategorySubject.Where(e => e.id == id).Include(e => e.Subjects).First();
            return a.Subjects;
            return _myContext.Subject.FromSqlRaw($"SELECT * FROM Subject WHERE Subject.Id_category = {id}").ToList();
        }
        
    }
}