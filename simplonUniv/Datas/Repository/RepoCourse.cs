using Contract;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Repository;
using Repository.Contract;
using simplonUniv.Entities;

namespace Repository
{
    public class RepoCourse : GenericRepository<Course>, IRepoCourse
    {

        public RepoCourse(IMyDbContext myContext): base(myContext)
        {
        }

       
    }
}