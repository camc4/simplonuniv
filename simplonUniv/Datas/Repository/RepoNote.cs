using Contract;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Repository;
using Repository.Contract;
using simplonUniv.Entities;

namespace Repository
{
    public class RepoNote : GenericRepository<Note>, IRepoNote
    {

        public RepoNote(IMyDbContext myContext): base(myContext)
        {
        }

       
    }
}