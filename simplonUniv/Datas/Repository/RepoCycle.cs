using Contract;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Repository;
using Repository.Contract;
using simplonUniv.Entities;

namespace Repository
{
    public class RepoCycle : GenericRepository<Cycle>, IRepoCycle
    {

        public RepoCycle(IMyDbContext myContext): base(myContext)
        {
        }

       
    }
}