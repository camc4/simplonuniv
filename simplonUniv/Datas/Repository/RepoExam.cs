using Contract;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Repository;
using Repository.Contract;
using simplonUniv.Entities;

namespace Repository
{
    public class RepoExam : GenericRepository<Exam>, IRepoExam
    {

        public RepoExam(IMyDbContext myContext): base(myContext)
        {
        }

       
    }
}